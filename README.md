# Quicktext website


## Python app

Ubuntu 13.10, initial setup:

```
sudo apt-get install python-dev python-virtualenv postgresql ssh
```

Create a new Postgre user:

```
username: q
password: q
```

And a new `q` database owned by `q`.

```
virtualenv .
source bin/activate
pip install fabric fabtools
```

```
cd ./fabfiles
fab -H {username}@localhost app.packages srv.postgres
```

Replace {username} with the system user.

```
cd ../
python setup.py develop
```

## Get latest migrations

```
. bin/activate
cd q/
python manage.py db upgrade head
```

## Run http server

Once done with the inital setup, run the server with:

```
. bin/activate
python q/manage.py runserver
```

[http://localhost:5000/](http://localhost:5000/)

## CSS/JS dev:

```
cd q/static/
sudo npm install -g grunt-cli bower
npm install
bower install
grunt
```
