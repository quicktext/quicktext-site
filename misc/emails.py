import sys
import csv
import subprocess
import re

from tld import get_tld


OUT_NAME = 'Email Provider'
EMAIL_REGEX = re.compile(r"[^@]+@([^@]+\.[^@;]+)")

IGNORE_DOMAINS = [
    'gmail.com',
    'yahoo.com',
    'yahoo.fr',
    'live.com',
    'hotmail.com',
    'hotmail.fr',
    'gymglish.com',
    'a9group.com',
    'a9english.com',
]


def provider(domain, memo={}):
    # ignore testing a few domains
    if any(i for i in IGNORE_DOMAINS if i in domain):
        return []
    try:
        tld = get_tld('http://' + domain.lower(), active_only=True, as_object=True).tld
    except Exception as e:
        print >> sys.stderr, e.message
        return
    if tld not in memo:
        memo[tld] = lookup(tld)

    return memo[tld]


def dig(name):
    cmd = ['dig', '+time=3', '+tries=2', '+short', 'mx', name]
    try:
        output = subprocess.check_output(cmd)
        return [
            line.split()[1].rstrip('.').lower()
            for line in output.splitlines()
        ]
    except Exception as e:
        return []


def lookup(domain):
    mx_list = dig(domain)
    if any(mx.endswith('.biz.mail.yahoo.com') for mx in mx_list):
        return 'yahoo biz'
    if any(mx.endswith('.mail.protection.outlook.com') for mx in mx_list):
        return 'office365'
    if any(mx.endswith('aspmx.l.google.com') for mx in mx_list):
        return 'google'


def main():
    with open(sys.argv[1], 'rb') as f:
        reader = csv.DictReader(f)
        writer = csv.DictWriter(sys.stdout, reader.fieldnames + [OUT_NAME])
        writer.writeheader()
        for line in reader:
            found = set()
            for k in line:
                if 'email' not in k.lower():
                    continue
                if not line[k]:
                    continue

                for email in line[k].split(','):
                    match = EMAIL_REGEX.match(email)
                    if match:
                        p = provider(match.group(1))
                        if p:
                            found.add(p)
            if found:
                line[OUT_NAME] = ', '.join(sorted(found))
                writer.writerow(line)


main()
