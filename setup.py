# Dummy egg used for testing
from setuptools import setup, find_packages

version = '1.0.0'

deps = [
    'Fabric',
    'Flask-HTTPAuth==2.2.0',
    'Flask-Login==0.2.7',
    'Flask-Mail==0.9.0',
    'Flask-Migrate',
    'Flask-Principal==0.4.0',
    'Flask-RESTful==0.2.11',
    'Flask-SQLAlchemy==1.0',
    'Flask-Script==0.6.3',
    'Flask-Security==1.6.9',
    'Flask-Social',
    'Flask-WTF==0.9.3',
    'Flask==0.10.1',
    'coverage==3.7',
    'fabtools',
    'google-api-python-client',
    'lxml',
    'nose==1.3.0',
    'oauth2client',
    'psycopg2',
    'raven',
    'setuptools',
    'six',
    'stripe',
    'uWSGI==1.9.21.1',
    'newrelic',
    'customerio',
    'celery',
    'redis'
]

setup(name='q',
      version=version,
      license='proprietary',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=deps,
)
