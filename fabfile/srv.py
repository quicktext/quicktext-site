"""Configure servers"""

from fabric.api import *

from fabtools import require
from fabtools.vagrant import vagrant

@task
@roles('app', 'monitor')
def deploy():
    """ Make sure the server is configured accordingly"""
    execute(packages)

@task
def packages():
    """ Install common packages """

    sudo("apt-get -qq update")

    # Require some Debian/Ubuntu packages
    require.deb.packages([
        'ack-grep',
        'git',
        'git-sh',
        'tmux',
        'vim',
        'build-essential',
        'iotop',
    ])
