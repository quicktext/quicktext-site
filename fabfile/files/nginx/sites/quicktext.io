server {
       listen 80;
       server_name %(server_name)s;
       rewrite ^ https://$server_name$request_uri? permanent;
}

server {
       listen 443;
       root %(docroot)s;
       server_name %(server_name)s %(server_alias)s;
       client_max_body_size 20M;

       if ($host = "%(server_alias)s") {
           rewrite  ^/(.*)$  https://%(server_name)s/$1  permanent;
       }

       ssl on;
       ssl_certificate /etc/ssl/private/%(ssl_cert)s;
       ssl_certificate_key /etc/ssl/private/%(ssl_key)s;
       ssl_protocols SSLv3 TLSv1;
       ssl_ciphers ALL:!aNULL:!ADH:!eNULL:!LOW:!EXP:RC4+RSA:+HIGH:+MEDIUM;

       location / { try_files $uri @q; }
       location @q {
           include uwsgi_params;
           uwsgi_pass 127.0.0.1:%(uwsgi_port)s;
       }
}
