var system = require('system');

if (system.args.length === 1) {
    console.log('URL and destination required!');
    phantom.exit();
} else {
    var url = system.args[1];
    var dest = system.args[2];
}

var page = require('webpage').create();
page.viewportSize = {'width': 1024, 'height': 800};

page.open(url, function() {
    var domain = url.replace('https://', '').replace('http://', '');
    var now = new Date();

    page.render(dest + '/' + domain + "-" + now.getFullYear() + "-" + now.getMonth() + 1 + "-" + now.getDate() + ".png");
    phantom.exit();
});
