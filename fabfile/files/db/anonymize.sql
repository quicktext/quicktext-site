-- Reseting user information
UPDATE "user" SET email = 'alex+user' || "user".id || '@quicktext.io', password='$6$rounds=100000$pr4D4ZOn3cH/lfTr$5eMb32bxeaItX92pb.7gMfdgMNSWUgf4aupRUvAP2a.YcwfDya7mOkc2iwFHzcRoALOUWAT4vjGmt4eOCzKRz.';
UPDATE "user_info" SET "value" = 'Unknown value';
UPDATE "user_info" SET "value" = 'Alex' || user_info.id WHERE "key" = 'given_name';
UPDATE "user_info" SET "value" = 'Plugaru' || user_info.id WHERE "key" = 'family_name';
UPDATE "user_info" SET "value" = 'Alex Plugaru' || user_info.id WHERE "key" = 'name';
UPDATE "user_info" SET "value" = TRUE WHERE "key" = 'verified_email';
UPDATE "connection" SET "provider_user_id" = '';
UPDATE "connection" SET "provider_id" = '';
UPDATE "connection" SET "access_token" = '';
UPDATE "connection" SET "secret" = '';

-- Subscriptions
UPDATE "subscription" SET "stripe_id" = NULL;
-- Invitations
UPDATE "invitation" SET "email" = 'alex+invitation' || invitation.id || 'quicktext.io';

-- Quicktext data
UPDATE "quicktext" SET "title" = 'Title' || quicktext.id;
UPDATE "quicktext" SET "body" = 'Body' || quicktext.id;
UPDATE "quicktext" SET "shortcut" = 'Shortcut' || quicktext.id;
UPDATE "quicktext" SET "subject" = 'Subject' || quicktext.id;
UPDATE "quicktext" SET "tags" = 'Tags' || quicktext.id;

UPDATE "quicktext_version" SET "title" = 'Title' || quicktext_version.id;
UPDATE "quicktext_version" SET "body" = 'Body' || quicktext_version.id;
UPDATE "quicktext_version" SET "shortcut" = 'Shortcut' || quicktext_version.id;
UPDATE "quicktext_version" SET "subject" = 'Subject' || quicktext_version.id;
UPDATE "quicktext_version" SET "tags" = 'Tags' || quicktext_version.id;

-- Stripe data - just delete it
DELETE FROM "stripe_event";