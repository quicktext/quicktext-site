""" Quicktext.io flask app """

import os

from fabric.api import *
from fabric.contrib.files import exists
from fabtools import require, cron, files

from q.config import AWS_BACKUP_SECRET_ACCESS_KEY, AWS_BACKUP_ACCESS_KEY_ID, WALE_S3_PREFIX


@task
@roles('app')
def packages():
    """ Install packages needed for the application """

    sudo("apt-get -qq update")

    # Require some Debian/Ubuntu packages
    require.deb.packages([
        'build-essential',
        'python-dev',
        'python-virtualenv',
        'libxml2',
        'libxslt-dev',
        'libpq-dev',
    ])


@task
@roles('app')
def install():
    """Install. Run only once!

    This will delete the app and install it again.

    """

    # install required packages prior to build
    execute(packages)
    execute(postgres)
    execute(nginx)

    if exists("quicktext.io"):
        run("rm -rf quicktext.io")
    if not exists("logs"):
        run("mkdir logs")
    run("git clone -q https://sasha@bitbucket.org/quicktext/quicktext-site.git quicktext.io")
    with cd("quicktext.io"):
        run("virtualenv .")
        with prefix("source bin/activate"):
            run("easy_install -U distribute")
            run("python setup.py develop")
    execute(migrate)


@task
@roles('app')
def update():
    """ Update the code """
    with cd("quicktext.io"):
        run("git pull -q origin master")
        with prefix("source bin/activate"):
            run("python setup.py develop")


@task
@roles('app')
def restart():
    """ Use supervisor to manage the application """

    home = "/home/%s/" % env.user
    app_home = home + "quicktext.io/"
    command = '{0}bin/newrelic-admin run-program \
{0}bin/uwsgi --socket 127.0.0.1:3030 --module q.q_app --callable app \
--processes 4 --threads 2 --stats 127.0.0.1:9090 \
--pidfile {1}/q.pid --disable-logging -M -t 120'.format(app_home, home)

    require.supervisor.process('quicktext',
                               command=command,
                               directory=home + 'quicktext.io/',
                               user=env.user,
                               stopsignal='INT',
                               environment='PYTHON_EGG_CACHE={1}.eggs,QUICKTEXT_PROD=1,NEW_RELIC_CONFIG_FILE={0}newrelic.ini'.format(
                                   app_home, home),
                               stdout_logfile='%s/logs/supervisor-quicktext.io.log' % home,
    )

    # restart supervisor process
    sudo("supervisorctl restart quicktext")


@task
@roles('app')
def migrate():
    """ Run migrations """
    with cd("quicktext.io/q"):
        with prefix("source ../bin/activate"):
            run("python manage.py db upgrade head")


@task
@roles('app')
def postgres():
    """ Install and cofigure postgres server """

    require.postgres.server()
    require.postgres.user('q', 'q')
    require.postgres.database('q', 'q')

    # backup setup
    require.deb.package("daemontools")
    sudo("/usr/bin/pip install wal-e")
    sudo("mkdir -p /etc/wal-e.d/env")
    sudo('echo "%s" > /etc/wal-e.d/env/AWS_SECRET_ACCESS_KEY' % AWS_BACKUP_SECRET_ACCESS_KEY)
    sudo('echo "%s" > /etc/wal-e.d/env/AWS_ACCESS_KEY_ID' % AWS_BACKUP_ACCESS_KEY_ID)
    sudo('echo "%s" > /etc/wal-e.d/env/WALE_S3_PREFIX' % WALE_S3_PREFIX)
    sudo('chown -R root:postgres /etc/wal-e.d')

    # edit postgresql.conf to include this:
    #
    #  wal_level = archive # hot_standby in 9.0+ is also acceptable
    #  archive_mode = on
    #  archive_command = 'envdir /etc/wal-e.d/env /usr/local/bin/wal-e wal-push %p'
    #  archive_timeout = 60


@task
@roles('app')
def celery():
    # install redis server
    require.deb.package('redis-server')

    home = "/home/%s/quicktext.io" % env.user
    cmd = "%s/bin/celery worker -A q.q_app.celery --loglevel=INFO " % home

    require.supervisor.process('quicktext_celeryd',
                               command=cmd,
                               directory=home,
                               user=env.user,
                               stdout_logfile="%s/../logs/celeryd.log" % home,
                               stderr_logfile="%s/../logs/celeryd-error.log" % home,
                               autostart='true',
                               autorestart='true',
                               startsecs=10,
                               stopwaitsecs=600,
                               killasgroup='true',
                               priority=998
    )
    sudo("supervisorctl restart quicktext_celeryd")

@task
@roles('app')
def resetdb(force=False):
    """ Get production database (anonymized) - get production db if we don't have a local one"""

    db_path = os.path.join(os.path.dirname(__file__), 'files/db/q.sql')
    anonymize_path = os.path.join(os.path.dirname(__file__), 'files/db/anonymize.sql')

    if not os.path.exists(db_path) or force:
        run('pg_dump -Uq q > /tmp/q.sql')
        get('/tmp/q.sql', db_path)
        run('rm /tmp/q.sql')

        local('dropdb -U q q')
        local('createdb -U q q')
        local('psql -U q q < %s' % db_path)
        local('psql -U q q < %s' % anonymize_path)
        local('pg_dump -Uq q> %s' % db_path)  # anonymized
        return

    local('dropdb -U q q')
    local('createdb -U q q')
    local('psql -U q q<%s' % db_path)


@task
@roles('app')
def nginx():
    """ Install and configure nginx

    Note that you need to copy the SSL certs/keys manually
    scp /path/to/quicktext_io_bundle.crt host:/etc/ssl

    """
    home = '/home/%s' % env.user
    site = 'quicktext.io'
    quicktext_io_template = open('files/nginx/sites/quicktext.io').read()

    # special conf for vagrant (no ssl)
    if env.user == 'vagrant':
        site = 'q-vagrant'
        home = "/home/vagrant"
        quicktext_io_template = open('files/nginx/sites/quicktext.io.vagrant').read()

    require.nginx.site(site,
                       template_contents=quicktext_io_template,
                       server_alias="www.%s" % site,
                       docroot='%s/quicktext.io' % home,
                       uwsgi_port='3030',
                       ssl_cert='quicktext.io.pem',
                       ssl_key='quicktext.io.key.nopass',
    )


@task
@roles('app')
def screenshots():
    """ Start a supervisor process that takes screenshots of the websites once a day """

    home = '/home/%s' % env.user
    screenshots_script = home + "/quicktext.io/fabfiles/files/screenshot.js"
    dest = home + '/screenshots/'
    url = "https://quicktext.io/"

    require.directory(dest)
    require.deb.packages(['build-essential', 'chrpath', 'git-core',
                          'libssl-dev', 'libfontconfig1-dev'])
    if not files.is_dir("phantomjs"):
        sudo("git clone git://github.com/ariya/phantomjs.git")
    with cd("phantomjs"):
        sudo("git checkout 1.9")
        sudo("./build.sh")

    cron.add_task("screenshot", "@daily", env.user,
                  '%s/phantomjs/bin/phantomjs %s %s %s' % (home, screenshots_script, url, dest))


@task
@roles('app')
def deploy():
    """ Deployment """
    execute(update)
    execute(restart)
    execute(migrate)
    execute(celery)
