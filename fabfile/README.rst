Fabric server/app config
========================

All the config is handled by fabric and fabtools.

To perform the initial installation use the fabric script.

Example::

    fab -Hhost1,host2 srv.deploy

This will perform the necessary steps to install and setup on `host1` and `host2`.

Testing
--------

To do your testing please use vagrant. Make sure you follow the installation steps
on the vagrant website.


Setup vagrant
+++++++++++++

To install a new vagrant machine (This will use the Vagrantfile in the dir)::

    vagrant up

Make sure you create a snapshot after you created this machine::

    vagrant plugin install vagrant-vbox-snapshot # install plugin
    vagrant snapshot take default init # take a snapshot just after the install

Now to start each time with a clean environment::

    vagrant snapshot go default init

Actual testing
+++++++++++++++

This will install on a vagrant box all the necessary packages and servers (nginx, postgresql, etc..)::

    fab srv.vagrant srv.deploy

Installing the application
==========================

The flask app can be installed just as easy::

    fab srv.vagrant app.install
    fab srv.vagrant app.deploy

This will first install the git repo and all packages, the deployment will
actually start the server
