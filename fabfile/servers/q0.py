import os

from fabric.api import *
from fabtools import require

@task
def install():
    require.system.hostname('q0.quicktext.io')
    require.user(
        'q',
        create_home=True,
        shell='/bin/bash',
        ssh_public_keys=[os.path.expanduser('~/.ssh/id_rsa.pub')]
    )
    require.sudoer('q')
