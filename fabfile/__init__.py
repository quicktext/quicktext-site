from fabric.api import env
env.use_ssh_config = True

env.roledefs = {
    'app': ['q@q0'],
    'monitor': ['ubuntu@q1']
}

# server configurations
import servers.q0

# generic configuration
import srv
import app
