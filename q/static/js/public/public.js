$(document).ready(function () {
    var $loginModal = $('.login-modal'),
        $forgotModal = $('.forgot-modal');

    var showLogin = function () {
        // close other modals
        $('.modal').modal('hide');

        // show login modal
        $loginModal.modal('show');

        var query = window.location.search;
        if (query.indexOf("?message=") !== -1) {
            $(".error-message").removeClass("hide");
            $(".error-message .error-body").html(decodeURI(query.split("?message=")[1]));
            $(".recover-password").removeClass("hide");
        }
    };

    var showForgot = function () {
        $('.modal').modal('hide');

        $forgotModal.modal('show');
        var query = window.location.search;
        if (query.indexOf("?message=") !== -1) {
            $(".error-message").removeClass("hide");
            $(".error-message .error-body").html(decodeURI(query.split("?message=")[1]));
        } else if (query.indexOf("?success=") !== -1) {
            $(".success-message").removeClass("hide");
            $(".success-message .message-body").html(decodeURI(query.split("?success=")[1]));
        }
    };

    window.onhashchange = function () {
        if (window.location.hash === '#login' || window.location.pathname === "/login") {
            showLogin();
        } else if (window.location.hash === '#forgot') {
            showForgot();
        }
    };
    window.onhashchange();

    // Inline chrome extension
    if (typeof chrome !== 'undefined' && chrome.app.isInstalled) {
        $('#extension-info').removeClass("hide");
        $('#install-extension').addClass("hide");
    }

});


// Signup page
$(document).ready(function () {
    var slider = $('input.slider');
    var helpBlock = slider.siblings('.help-block');
    var helpBlockText = helpBlock.html();

    if (slider.length) {
        helpBlock.hide();

        var sliderVal = parseInt(slider.val(), 10);
        $('.slider-value').text(sliderVal);

        slider.slider({
            'min': 1,
            'max': 10,
            'value': sliderVal,
            'range': false
        }).on('slide', function (e) {
            var amount = e.value * PLAN_UNIT_COST;
            $('#total-amount').text(amount.toFixed(2));
            $('.slider-value').text(e.value);
        }).on('slideStop', function (e) {
            if (e.value === 1) {
                helpBlock.html(helpBlockText);
                helpBlock.show();
            } else if (e.value === 10) {
                helpBlock.html('Need more? Contact us at <a href="mailto://support@quicktext.io">support@quicktext.io</a>');
                helpBlock.show();
            } else {
                helpBlock.hide();
            }

        });


    }

    $('#cc-number').on('focus', function () {
        $('.cc-types').hide();
    });

    var signupForm = $('#signup-form');
    if (signupForm.length) {
        signupForm.submit(function (event) {
            var $form = $(this);
            $form.find('.cc-errors').addClass('hide');

            // Disable the submit button to prevent repeated clicks
            $form.find('button').prop('disabled', true);

            Stripe.card.createToken($form, function (status, response) {
                if (response.error) {
                    // Show the errors on the form
                    $form.find('.cc-errors').removeClass('hide');
                    $form.find('.cc-errors').text(response.error.message);
                    $form.find('button').prop('disabled', false);
                } else {
                    // response contains id and card, which contains additional card details
                    var token = response.id;
                    // Insert the token into the form so it gets submitted to the server
                    $form.find("#stripe_token").val(token);
                    // and submit
                    $form.get(0).submit();
                }
            });

            // Prevent the form from submitting with the default action
            return false;
        });

        signupForm.card({
            // a selector or jQuery object for the container
            // where you want the card to appear
            container: '.card-wrapper', // *required*
            numberInput: 'input#cc-number', // optional — default input[name="number"]
            cvcInput: 'input#cc-cvc', // optional — default input[name="cvc"]
            nameInput: 'input#name', // optional - defaults input[name="name"]

            width: 200, // optional — default 350px
            formatting: true, // optional - default true

            // Strings for translation - optional
            messages: {
                validDate: 'valid\ndate', // optional - default 'valid\nthru'
                monthYear: 'mm/yy', // optional - default 'month/year'
            }
        });
    }
});
