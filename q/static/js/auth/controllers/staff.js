qApp.controller('StaffCtrl', function ($rootScope, $scope, $location, StaffUserService) {
    $scope.users = [];

    $scope.refreshUsers = function () {
        StaffUserService.get().then(function (users) {
            $scope.users = users;
        });
    };
    $scope.refreshUsers();

    $scope.impersonate = function() {
        StaffUserService.impersonate(this.user.id).success(function(){
            $location.path('/');
        });
    };
});

