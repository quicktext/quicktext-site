qApp.controller('AccountCtrl', function ($scope, $rootScope, AccountService, MemberService, SubscriptionService) {
    var self = this;

    $scope.saveAccount = function () {
        mixpanel.track("Saved Account Settings");
        AccountService.update($rootScope.user).then(function () {
            $(".updated-account-message").removeClass("hide");
        });
    };

    // Members
    $scope.members = [];
    $scope.sendNotification = true;

    $scope.refreshMembers = function () {
        var getMembers = function () {
            MemberService.members().then(function (members) {
                for (var i = 0; i < members.length; i++) {
                    $scope.members[i] = members[i];
                }
            });
        };

        if ($rootScope.user.is_customer) {
            // check the active subscription first
            SubscriptionService.getActiveSubscription().then(function (sub) {
                $scope.members = [];

                var defaults = {
                    'id': '',
                    'name': '',
                    'email': ''
                };

                for (var i = 0; i < sub.quantity - 1; i++) {
                    $scope.members.push(angular.copy(defaults));
                }
                getMembers();
            });
        } else {
            getMembers();
        }

    };

    $scope.refreshMembers();

    $scope.saveMembers = function () {
        mixpanel.track("Updated Members");
        _.each($scope.members, function (member) {
            if (!(member.name && member.email)) {
                return;
            }

            member.sendNotification = $scope.sendNotification;
            MemberService.update(member).then(function () {
                $scope.formErrors = null;
                $scope.refreshMembers();
                $('.modal').modal('hide');
            }, function (errors) {
                $scope.formErrors = errors;
            });
        });
    };

    $scope.deleteMember = function () {
        if (confirm("Are you sure you want to delete this member from your team?")) {
            mixpanel.track("Deleted Member");
            MemberService.delete(this.member).then(function () {
                $scope.refreshMembers();
            });
        }
    };
});
