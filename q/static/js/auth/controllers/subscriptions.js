qApp.controller('SubscriptionsCtrl', function($scope, $rootScope, SubscriptionService) {

    $scope.plans = {};
    $scope.subscriptions = [];
    $scope.stripeKey = "";
    $scope.preferredCurrency = "";
    $scope.quantity = 1;
    $scope.paymentError = "";

    SubscriptionService.plans().then(function(data) {
        $scope.plans = data.plans;
        $scope.stripeKey = data.stripe_key;
        $scope.preferredCurrency = data.preferred_currency;
        $scope.email = data.email;
    });

    $scope.reloadSubscriptions = function() {
        SubscriptionService.subscriptions().then(function(data) {
            $scope.subscriptions = data;
        });
    };

    $scope.reloadSubscriptions();

    $scope.subscribe = function(quantity) {
        $scope.selectedPlan = this.plan || $scope.selectedPlan;
        $scope.quantity = quantity || $scope.quantity;


        var handler = StripeCheckout.configure({
            key: $scope.stripeKey,
            image: '/static/img/icon128.png',
            token: function(token) {
                // Use the token to create the charge with server-side.
                SubscriptionService.addSubscription($scope.selectedPlan.sku, $scope.quantity, token, function() {
                    // success
                    $scope.paymentError = "";
                    $scope.reloadSubscriptions();
                }, function(res) {
                    // failure with an error (if any)
                    $scope.paymentError = res.data;
                });
            }
        });

        handler.open({
            name: 'Quicktext',
            description: $scope.selectedPlan.name + ' Subscription',
            amount: $scope.selectedPlan.amount * 100 * parseInt($scope.quantity, 10),
            email: $scope.email,
            currency: $scope.selectedPlan.currency,
            opened: function(){
                $(".subscribe-button").button('loading');
            },
            closed: function(){
                $(".subscribe-button").button('reset');
            }
        });
    };

    $scope.showDeleteAccountModal = function() {
        mixpanel.track("Opened delete account modal");
        $("#delete-account-modal").modal();
    };

    $scope.deleteAccount = function() {
        mixpanel.track("Deleted account");
        window.location = "/";
    };
});
