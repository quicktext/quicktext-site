qApp.controller('StatsCtrl', function ($scope, StatsService) {
    var self = this;

    $scope.data = {'count': 0, 'times': {}};
    $scope.chartType = 'line-dotted';
    $scope.dateFormat = d3.time.format.utc('%Y-%m-%d');
    $scope.endDate = new Date();
    $scope.startDate = d3.time.day.offset($scope.endDate, -7);

    $scope.setChartType = function (chartType) {
        $scope.chartType = chartType;
    };

    $scope.$watch('chartType', function () {
        if ($scope.chartType && $scope.chart) {
            $scope.chart.setType($scope.chartType);
        }
    });

    $scope.refreshStats = function () {
        var query = {
            startDate: $scope.startDate,
            endDate: $scope.endDate
        };
        StatsService.get(query).then(function (data) {
            $scope.data = data;

            var tt = document.createElement('div'),
                leftOffset = -(~~$('html').css('padding-left').replace('px', '') + ~~$('body').css('margin-left').replace('px', '')),
                topOffset = -32;
            $(tt).addClass('ex-tooltip');
            $(tt).hide();
            document.body.appendChild(tt);

            var opts = {
                "paddingTop": 10,
                "dataFormatX": function (x) {
                    return d3.time.format('%Y-%m-%d').parse(x);
                },
                "tickFormatX": function (x) {
                    return d3.time.format('%A')(x);
                },
                "mouseover": function (d, i) {
                    var pos = $(this).offset();
                    $(tt).text(d3.time.format('%A')(d.x) + ': ' + d.y)
                        .css({top: topOffset + pos.top, left: pos.left + leftOffset})
                        .show();
                },
                "mouseout": function (x) {
                    $(tt).hide();
                }
            };
            $scope.chart = new xChart('line-dotted', data, '#chart', opts);


            // Create a legend
            var legendData = {
                "xScale": "linear",
                "yScale": "linear",
                "main": [ ]
            };

            for (var i = 0; i < data.main.length; i++) {
                legendData.main.push(
                    {
                        "label": data.main[i].label,
                        "className": data.main[i].className + "-legend",
                        "data": [
                            {
                                "x": 0,
                                "y": i
                            },
                            {
                                "x": 1,
                                "y": i
                            }
                        ]
                    });
            }

            var legendOpts = {
                //"paddingBottom":0,
                //"axisPaddingTop":3,
                //"axisPaddingLeft": 30,
                "axisPaddingRight": 0,
                "paddingTop": 15,
                "paddingLeft": 98,
                "tickFormatY": function (y) {
                    if (typeof(legendData.main[y]) != "undefined") {
                        return legendData.main[y].label;
                    }
                    //return "oops";
                },
                "tickHintY": legendData.main.length,
                "tickHintX": 0,
                "xMin": 0,
                "xMax": 1,
            };
            var legend = new xChart('line-dotted', legendData, '#legend', legendOpts);
        });

    };
    $scope.refreshStats();

    $scope.$watch('startDate', function() { $scope.refreshStats(); });
    $scope.$watch('endDate', function() { $scope.refreshStats(); });

});
