qApp.controller('ListCtrl', function ($scope, $rootScope, $routeParams, $location, $cookieStore, $upload, $filter, QuicktextService, QuicktextSharingService, MemberService, SubscriptionService) {

    $scope.quicktexts = [];
    $scope.filteredQuicktexts = [];
    $scope.tags = [];
    $scope.filterTags = [];

    $scope.selectedQuicktexts = [];
    $scope.showActionsForSelected = false; // since nothing is selected don't display the action buttons
    $scope.shareData = {
        emails: '',
        message: ''
    };

    // Limit of quicktexts to be shown by default
    $scope.limitQuicktexts = 42;

    $scope.$on('$viewContentLoaded', function () {
        // Inline chrome extension is installed !?
        if (typeof chrome !== 'undefined' && !chrome.app.isInstalled && !$cookieStore.get('installModal')) {
            window.setTimeout(function () {
                $('#installation-modal').modal('show');
            }, 500);

            // hide the modal in the future
            $cookieStore.put('installModal', 'hide');
        }
    });


    // by default the load more button is disabled
    $('.load-more').hide();

    $scope.reloadQuicktexts = function () {
        QuicktextService.quicktexts().then(function (ret) {
            $scope.quicktexts = ret.quicktexts;
            $scope.tags = ret.tags;
        });
    };

    $scope.reloadQuicktexts();

    // apply filters to the list of quicktexts
    var filterQuicktexts = function () {
        // apply the text search filter
        $scope.filteredQuicktexts = $filter('filter')($scope.quicktexts, $scope.searchText);
        // apply the tag serach filter
        $scope.filteredQuicktexts = $filter('tagFilter')($scope.filteredQuicktexts, $scope.filterTags);

        $scope.focusIndex = 0;

        if ($scope.filteredQuicktexts.length < $scope.limitQuicktexts) {
            $('.load-more').hide();
            $scope.limitQuicktexts = 42;
        } else {
            $('.load-more').show();
        }
    };

    $scope.$watch('searchText', filterQuicktexts);
    $scope.$watch('filterTags', filterQuicktexts, true);

    $scope.$watch('quicktexts', function () {
        if ($scope.quicktexts && $scope.quicktexts.length) {
            // trigger filterQuicktexts to update filtered quicktexts
            filterQuicktexts();
        }
    });

    // A tag was clicked, filter quicktexts
    $scope.toggleFilterTag = function () {
        var index = $scope.filterTags.indexOf(this.tag);
        if (index === -1) {
            $scope.filterTags.push(this.tag);
        } else {
            $scope.filterTags.splice(index, 1); // remove from tags
        }
    };

    // Load more quicktexts action
    $scope.loadMore = function () {
        $scope.limitQuicktexts += 42;
        if ($scope.limitQuicktexts > $scope.filteredQuicktexts.length) {
            $(".load-more").hide();
            $scope.limitQuicktexts = 42;
        }
    };

    // Quicktext Selection
    $scope.toggleQuicktext = function () {
        var quicktext = _.findWhere($scope.selectedQuicktexts, {'id': this.quicktext.id});
        if (quicktext) {
            $scope.selectedQuicktexts = _.filter($scope.selectedQuicktexts, function (q) {
                return q.id !== quicktext.id;
            });
        } else {
            $scope.selectedQuicktexts.push(this.quicktext);
        }
    };

    $scope.selectedAll = false;

    $scope.toggleSelectAll = function () {
        $scope.selectedAll = !$scope.selectedAll;

        _.each($scope.filteredQuicktexts, function (qt) {
            qt.selected = $scope.selectedAll;
        });
        $scope.selectedQuicktexts = $scope.selectedAll ? angular.copy($scope.filteredQuicktexts) : [];
    };

    // Show the form for adding a new quicktext or creating one
    $scope.showForm = function (id) {
        var defaults = {
            'id': '',
            'key': '',
            'subject': '',
            'shortcut': '',
            'title': '',
            'tags': '',
            'body': ''
        };
        if (!this.quicktext) { // new qt
            mixpanel.track("New Quicktext Form");
            $scope.selectedQt = angular.copy(defaults);
        } else { // update qt
            mixpanel.track("Edit Quicktext Form");
            QuicktextService.get(this.quicktext.id).then(function (qt) {
                $scope.selectedQt = qt;
            });
        }

        $('#quicktext-modal').modal();
    };

    // Delete one or multiple quicktexts
    $scope.deleteQt = function () {
        var r, quicktexts = [];
        if (this.quicktext) {
            r = confirm("Are you sure you want to delete '" + this.quicktext.title + "' Quicktext?");
            quicktexts = [this.quicktext];
        } else {
            r = confirm("Are you sure you want to delete " + $scope.selectedQuicktexts.length + " Quicktexts?");
            quicktexts = this.selectedQuicktexts;
        }
        if (r === true) {
            QuicktextService.delete(quicktexts).then(function () {
                $scope.reloadQuicktexts();
            });
        }
    };

    // Save a quicktext, perform some checks before
    $scope.saveQt = function () {
        if (!$scope.selectedQt.title) {
            alert("Please enter a Title");
            return;
        }

        if (!$scope.selectedQt.body) {
            alert("Please enter a Quicktext Template");
            return;
        }
        QuicktextService.update($scope.selectedQt).then(function () {
            $scope.reloadQuicktexts();
            // hide teh modal
            $('.modal').modal('hide');
        });
    };

    // Save a quicktext, perform some checks before
    $scope.duplicateQt = function () {
        if (!$scope.selectedQt.title) {
            alert("Please enter a Title");
            return false;
        }

        if (!$scope.selectedQt.body) {
            alert("Please enter a Quicktext Template");
            return false;
        }

        $scope.selectedQt.id = null;
        QuicktextService.update($scope.selectedQt).then(function () {
            $scope.reloadQuicktexts();

            mixpanel.track("Duplicated Quicktext");
            // hide teh modal
            $('.modal').modal('hide');
        });
    };

    // Sharing quicktexts
    $scope.sharing = {
        "acl": [],
        "members": []
    };

    $scope.reloadSharing = function () {
        QuicktextSharingService.list($scope.selectedQuicktexts).then(function (result) {
            // Show a user only once
            var acl = [];
            var userIds = []; // Show each user only once

            _.each(result, function (row) {
                if (!_.contains(userIds, row.target_user_id)) {
                    userIds.push(row.target_user_id);
                    acl.push(row);
                }
            });
            $scope.sharing.acl = acl;
        });
    };

    // show the share modal
    $scope.showShareModal = function (id) {
        if (!$scope.selectedQuicktexts.length) {
            alert("Please select at least one quicktext");
            return;
        }

        $scope.reloadSharing();

        // get all members and populate the selectize list
        MemberService.members().then(function (members) {
            var options = [];
            _.each(members, function (member) {
                options.push({
                    'user_id': member.user_id,
                    'email': member.email,
                    'name': member.name
                });
            });

            var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' +
                '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

            $('#qt-invite-people').selectize({
                persist: false,
                maxItems: null,
                valueField: 'email',
                labelField: 'name',
                searchField: ['name', 'email'],
                options: options,
                render: {
                    item: function (item, escape) {
                        return '<div>' +
                            (item.name ? '<strong class="name">' + escape(item.name) + '</strong> ' : '') +
                            (item.email ? '<span class="email">' + escape(item.email) + '</span>' : '') +
                            '</div>';
                    },
                    option: function (item, escape) {
                        var label = item.name || item.email;
                        var caption = item.name ? item.email : null;
                        return '<div>' +
                            '<span class="label">' + escape(label) + '</span>' +
                            (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
                            '</div>';
                    }
                },
                createFilter: function (input) {
                    var match, regex;

                    // email@address.com
                    regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');
                    match = input.match(regex);
                    if (match) return !this.options.hasOwnProperty(match[0]);

                    // name <email@address.com>
                    regex = new RegExp('^([^<]*)<' + REGEX_EMAIL + '>$', 'i');
                    match = input.match(regex);
                    if (match) return !this.options.hasOwnProperty(match[2]);

                    return false;
                },
                create: function (input) {
                    if ((new RegExp('^' + REGEX_EMAIL + '$', 'i')).test(input)) {
                        return {email: input};
                    }
                    var match = input.match(new RegExp('^([^<]*)<' + REGEX_EMAIL + '\\>$', 'i'));
                    if (match) {
                        return {
                            email: match[2],
                            name: $.trim(match[1])
                        };
                    }
                    alert('Invalid email address.');
                    return false;
                }
            });
        });


        $('#quicktext-share-modal').modal();
    };

    $scope.shareQuicktexts = function () {
        // Only edit permission for now - meaning that
        QuicktextSharingService.create($scope.selectedQuicktexts, $scope.shareData, 'edit').then(function () {
            $scope.reloadSharing();
        });
    };

    $scope.shareMessage = "";

    $scope.revokeAccess = function () {
        QuicktextSharingService.delete($scope.selectedQuicktexts, this.share.target_user_id).then(function () {
            $scope.reloadSharing();
        });
    };


    // Quicktext Import
    $scope.showImportModal = function () {
        mixpanel.track("Opened Import Modal");
        $('#import-modal').modal();
    };

    // Importing Thunderbird quicktexts
    $scope.onFileSelect = function ($files) {
        mixpanel.track("Imported Quicktext");

        $('.progress').removeClass('hide');
        $('.progress-bar').attr('aria-valuenow', '98');
        $('.progress-bar').animate({'width': "98%"}, 1500);

        var file = $files[0];
        $scope.upload = $upload.upload({
            url: qApp.API_BASE_URL + 'quicktexts/import',
            file: file
        }).progress(function (evt) {
            //TODO: try to find a way to make this work in the future
            console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
        }).success(function (data, status, headers, config) {
            // file is uploaded successfully
            $('#import-modal').modal('hide');
            $scope.reloadQuicktexts();
        });
    };
});

