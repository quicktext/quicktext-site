qApp.service('StatsService', function($q, $resource) {
    var self = this;
    var statsResource = $resource(qApp.API_BASE_URL + 'stats', {}, {
        get: {
            method: "GET"
        }
    });

    self.get = function(options) {
        var deferred = $q.defer();
        var timeFormat = d3.time.format.utc('%Y-%m-%d');
        var query = {
          startDate: timeFormat(options.startDate),
          endDate: timeFormat(options.endDate)
        };
        statsResource.get(query, function(data) {
            var account = [];
            var team = [];
            _.each(data.words.account, function(value, date){
                account.push({
                    'x': date,
                    'y': value
                });
            });

            _.each(data.words.team, function(value, date){
                team.push({
                    'x': date,
                    'y': value
                });
            });

            deferred.resolve({
                "xScale": "time",
                "yScale": "linear",
                "main": [{
                    "className": ".user-words",
                    "label": "Your Account",
                    "data": account
                }, {
                    "className": ".team-words",
                    "label": "Team Average",
                    "data": team
                }],
                "times": data.data.times,
                "count": data.data.count,
                "wpm": data.data.wpm
            });
        });
        return deferred.promise;
    };
});
