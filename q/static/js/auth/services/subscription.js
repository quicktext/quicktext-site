qApp.service('SubscriptionService', function($q, $resource) {
    var self = this;
    var subResource = $resource(qApp.API_BASE_URL + 'subscriptions');
    var planResource = $resource(qApp.API_BASE_URL + 'plans');

    self.plans = function() {
        var deferred = $q.defer();
        var planData = planResource.get(function() {
            deferred.resolve(planData);
        });
        return deferred.promise;
    };

    self.subscriptions = function() {
        var deferred = $q.defer();
        var subscriptions = subResource.query(function() {
            deferred.resolve(subscriptions);
        });
        return deferred.promise;
    };

    self.addSubscription = function(sku, quantity, token, success, failure) {
        var subscription = new subResource();
        subscription.sku = sku;
        subscription.quantity = quantity;
        subscription.token = token;
        subscription.$save(success, failure);
    };

    self.getActiveSubscription = function () {
        var deferred = $q.defer();
        self.subscriptions().then(function(subscriptions){
            _.each(subscriptions, function(sub){
                if (sub.active) {
                    deferred.resolve(sub);
                }
            });
        });
        return deferred.promise;
    };
});

