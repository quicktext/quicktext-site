qApp.service('QuicktextService', function($q, $resource) {
    var self = this;
    self.qRes = $resource(qApp.API_BASE_URL + 'quicktexts/:quicktextId', {
        quicktextId: '@id'
    }, {
        update: {
            method: "PUT"
        },
        delete: {
            method: "DELETE"
        }
    });

    self.quicktexts = function() {
        var deferred = $q.defer();
        // get only the latest versions of quicktexts
        self.qRes.query(function(data) {
            var quicktextList = [];
            _.each(data, function(qt) {
                quicktextList.push({
                    id: qt.id,
                    created_datetime: qt.created_datetime,
                    updated_datetime: qt.versions[0].created_datetime,
                    title: qt.versions[0].title,
                    subject: qt.versions[0].subject,
                    shortcut: qt.versions[0].shortcut,
                    tags: qt.versions[0].tags,
                    body: qt.versions[0].body
                });
            });
            deferred.resolve({
                'quicktexts': quicktextList,
                'tags': self.allTags(quicktextList)
            });
        });
        return deferred.promise;
    };

    // get quicktext object given an id or null
    self.get = function(id) {
        var deferred = $q.defer();
        self.qRes.get({quicktextId: id}, function(qt) {
            var ret = {
                'id': id,
                'created_datetime': qt.created_datetime,
                'updated_datetime': qt.versions[0].created_datetime,
                'title': qt.versions[0].title,
                'subject': qt.versions[0].subject,
                'shortcut': qt.versions[0].shortcut,
                'tags': qt.versions[0].tags,
                'body': qt.versions[0].body,
            };
            deferred.resolve(ret);
        });
        return deferred.promise;
    };

    // given a string with tags give a clean list
    // remove spaces, duplicates and so on
    self._clean_tags = function(tags) {
        var tArray = _.filter(tags.split(','), function(tag) {
            if (tag.trim() !== '') {
                return true;
            }
        });
        tags = _.unique(_.map(tArray, function(t) {
            return t.trim();
        })).join(', ');
        return tags;
    };

    self._copy = function(source, target) {
        for (var k in source) {
            // ignore the id
            if (k === 'id') {
                continue;
            }
            if (k === 'tags') {
                target[k] = self._clean_tags(source[k]);
            } else {
                target[k] = source[k];
            }
        }
        return target;
    };

    // update a quicktext and try to sync
    self.update = function(qt) {
        var deferred = $q.defer();
        var qtRes = new self.qRes();
        qtRes = self._copy(qt, qtRes);
        if (qt.id) {
            self.qRes.update({quicktextId: qt.id}, qtRes, function(){
                mixpanel.track('Updated Quicktext');
                deferred.resolve(qt);
            });
        } else {
            qtRes.$save(function(){
                mixpanel.track('Created Quicktext');
                deferred.resolve(qt);
            });
        }

        return deferred.promise;
    };

    // delete a quicktext and try to sync
    self.delete = function(quicktexts) {
        var deferred = $q.defer();

        var qt = new self.qRes();
        qt.quicktext_ids = _.map(quicktexts, function(qt){return qt.id;});
        qt.action = "delete";
        console.log(qt);

        qt.$update(function(){
            mixpanel.track('Deleted Quicktexts');
            deferred.resolve();
        });
        return deferred.promise;
    };

    // get all tags from a quicktext
    self.tags = function(qt) {
        var retTags = [];
        _.each(qt.tags.split(","), function(tag) {
            retTags.push(tag.replace(/ /g, ""));
        });
        return retTags;
    };

    // get all tags
    self.allTags = function(quicktexts) {
        var tagsCount = {};
        _.each(quicktexts, function(qt) {
            _.each(qt.tags.split(","), function(tag) {
                tag = tag.replace(/ /g, "");
                if (!tag) {
                    return;
                }
                if (!tagsCount[tag]) {
                    tagsCount[tag] = 1;
                } else {
                    tagsCount[tag]++;
                }
            });
        });
        return tagsCount;
    };

});
