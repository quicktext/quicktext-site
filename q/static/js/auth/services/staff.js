qApp.service('StaffUserService', function($q, $resource, $http) {
    var self = this;
    var userResource = $resource(qApp.API_BASE_URL + 'staff/users', {}, {
        get: {
            method: "GET"
        }
    });

    self.get = function() {
        var deferred = $q.defer();
        userResource.query(function(data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    };

    self.impersonate = function(user_id) {
        return $http.get(qApp.AUTH_BASE_URL + 'staff/impersonate/' + user_id);
    };
});
