var qApp;
qApp = angular.module('qApp', [
  'ngRoute',
  'ngResource',
  'ngAnimate',
  'ngCookies',
  'angular-md5',
  'angularFileUpload',
  'datePicker'
]).config(function($routeProvider) {

    $routeProvider
        .when('/dashboard', {
            controller: 'DashboardCtrl',
            templateUrl: 'static/js/auth/views/dashboard.html'
        })
        .when('/quicktexts', {
            controller: 'ListCtrl',
            templateUrl: 'static/js/auth/views/quicktext/quicktexts.html'
        })
        .when('/account', {
            controller: 'AccountCtrl',
            templateUrl: 'static/js/auth/views/account/account.html'
        })
        .when('/subscriptions', {
            controller: 'SubscriptionsCtrl',
            templateUrl: 'static/js/auth/views/account/subscriptions.html'
        })
        .when('/staff', {
            controller: 'StaffCtrl',
            templateUrl: 'static/js/auth/views/staff/users.html'
        })
        .when('/stats', {
            controller: 'StatsCtrl',
            templateUrl: 'static/js/auth/views/stats.html'
        })
        .otherwise({
            redirectTo: '/quicktexts'
        });

});

qApp.run(function($rootScope, $location, AccountService) {

    mixpanel.track('App Loaded');

    $rootScope.$on('$routeChangeStart', function(next, current) {
        $rootScope.path = $location.path();
    });

    // init dom plugins
    var initDom = function() {

        // init bootstrap elements
        $('[data-toggle=tooltip]').tooltip();
        $('[data-toggle=popover]').popover();

        //put focus on the first text input when opening modals
        $('.modal').on('shown.bs.modal', function() {
            $(this).find('input[type!="hidden"]:first').focus();
        });
    };

    $rootScope.$on('$viewContentLoaded', initDom);
    $rootScope.$on('$includeContentLoaded', initDom);

    // User properties, can be global
    $rootScope.user = {};
    $rootScope.refreshUserData = function() {
        AccountService.get().then(function(user) {
            $rootScope.user = {
                'id': user.id,
                'email': user.email,
                'name': user.info.name,
                'share_all': user.info.share_all === 'true',
                'is_staff': user.is_staff,
                'is_customer': user.is_customer
            };

            mixpanel.identify(user.id);
            mixpanel.alias(user.email);

            mixpanel.people.set({
                '$first_name': user.info.given_name,
                '$last_name': user.info.family_name,
                '$email': user.email
            });
            mixpanel.people.set_once({
                'First Login': new Date()
            });
        });
    };
    $rootScope.refreshUserData();
});
