// Truncate and end with ...
qApp.filter('truncate', function() {
    return function(text, length, end) {
        if (isNaN(length)) {
            length = 100;
        }

        if (end === undefined) {
            end = "...";
        }

        if (text.length <= length || text.length - end.length <= length) {
            return text;
        } else {
            return String(text).substring(0, length - end.length) + end;
        }
    };
});

// replace \n by <br />
qApp.filter('newlines', function() {
    return function(text) {
        return text.replace("\n", "<br />");
    };
});

// tell angular that an output is safe
qApp.filter('safe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});

// Filter quicktexts by tags
qApp.filter('tagFilter', function(QuicktextService) {
    return function(quicktexts, filterTags) {
        if (filterTags && filterTags.length) {
            return _.filter(quicktexts, function(qt) {
                var tags = QuicktextService.tags(qt);
                return _.intersection(filterTags, tags).length === filterTags.length;

            });
        } else {
            return quicktexts;
        }
    };
});

qApp.filter('range', function() {
    return function(input, min, max) {
        min = parseInt(min); //Make string input int
        max = parseInt(max);
        for (var i = min; i < max; i++)
            input.push(i);
        return input;
    };
});

qApp.filter('title', function() {
    return function(input) {
        var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;

        return input.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function(match, index, title) {
            if (index > 0 && index + match.length !== title.length &&
                match.search(smallWords) > -1 && title.charAt(index - 2) !== ":" &&
                (title.charAt(index + match.length) !== '-' || title.charAt(index - 1) === '-') &&
                title.charAt(index - 1).search(/[^\s-]/) < 0) {
                return match.toLowerCase();
            }

            if (match.substr(1).search(/[A-Z]|\../) > -1) {
                return match;
            }

            return match.charAt(0).toUpperCase() + match.substr(1);
        });
    };

});

qApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    };
});
