'use strict'

module.exports = function(grunt) {
    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    var jsDeps = {
        public: [
            'bower_components/jquery/dist/jquery.js',
            'bower_components/fancybox/source/jquery.fancybox.js',
            'bower_components/fancybox/source/helpers/jquery.fancybox-media.js',
            'bower_components/bootstrap/dist/js/bootstrap.js',
            'bower_components/odometer/odometer.js',
            'bower_components/seiyria-bootstrap-slider/js/bootstrap-slider.js',
            'bower_components/card/lib/js/card.js',

            'js/public/**/*.js'
        ],
        auth: [
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap/dist/js/bootstrap.js',
            'bower_components/d3/d3.js',
            'bower_components/bower-xcharts/xcharts.js',
            'bower_components/sifter/sifter.js',
            'bower_components/microplugin/src/microplugin.js',
            'bower_components/selectize/dist/js/selectize.js',
            'bower_components/underscore/underscore.js',

            'bower_components/angular/angular.js',
            'bower_components/angular-route/angular-route.js',
            'bower_components/angular-animate/angular-animate.js',
            'bower_components/angular-resource/angular-resource.js',
            'bower_components/angular-cookies/angular-cookies.js',
            'bower_components/angular-md5/angular-md5.js',
            'bower_components/angular-datepicker/dist/index.js',
            'bower_components/ng-file-upload/angular-file-upload.js',

            'js/auth/**/*.js'
        ]
    };

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      public: {
        src: jsDeps.public,
        dest: 'js/_build/public.js'
      },
      auth: {
        src: jsDeps.auth,
        dest: 'js/_build/auth.js'
      }
    },
    jshint: {
      dev: [
        'js/public/**/*.js',
        'js/auth/**/*.js',
      ],
      options: {
        multistr: true
      }
    },
    stylus: {
        compile: {
            options: {
                'include css': true,
                'paths': ['css/'],
            },
            files: {
                'css/_build/public.css': 'css/public.styl',
                'css/_build/auth.css': 'css/auth.styl'
            }
        }
    },
    watch: {
        scripts: {
            files: ['js/**/*.js'],
            tasks: ['jshint', 'concat'],
            options: {
                spawn: false,
                livereload: true,
            },
        },
        styles: {
            files: ['css/**/*.styl'],
            tasks: ['stylus'],
            options: {
                spawn: false,
                livereload: true,
            },
        },
    },
    // Put files not handled in other tasks here
    copy: {
      development: {
        files: [{
          expand: true,
          flatten: true,
          dot: true,
          cwd: 'bower_components/font-awesome/fonts/',
          dest: 'css/fonts',
          src: [
            '*'
          ]
        }]
      }
    }
  });

  // Default task(s).
  grunt.registerTask('js', ['jshint', 'concat']);
  grunt.registerTask('css', ['stylus']);
  grunt.registerTask('default', ['js', 'css', 'copy', 'watch']);
};
