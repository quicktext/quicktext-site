# -*- coding: utf-8 -*-
from q.testing import QTestCase
from q.q_app import app
from q.models.user import User


class UserTestCase(QTestCase):
    def test_create(self):
        with app.app_context():
            email = 'test-new@quicktext.io'
            password = 'password'
            user = User.create(app, email, password)
            self.assertEqual(user.email, email)
            self.assertEqual(user.info(), {})

            email = 'test-new2@quicktext.io'
            extra = {'given_name': u'Hello'}
            user = User.create(app, email, password, extra=extra)
            self.assertEqual(user.email, email)
            self.assertEqual(user.info(), extra)

            email = 'test-new3@quicktext.io'
            extra = {'given_name': u'Thân'}
            user = User.create(app, email, password, extra=extra)
            self.assertEqual(user.email, email)
            self.assertEqual(user.info(), extra)