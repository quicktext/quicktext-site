from datetime import datetime, timedelta

from q.testing import QTestCase
from q.q_app import app, db
from q.models.customer import Customer, Subscription, Member
from q.models.user import User


class CustomerTestCase(QTestCase):
    def test_manage_subscriptions(self):
        with app.app_context():
            customer = Customer()
            customer.user_id = 1
            customer.stripe_id = ""
            customer.created_datetime = datetime.utcnow()
            db.session.add(customer)
            db.session.commit()

            # first test with bonus subscription - that is still active
            sub = Subscription()
            sub.customer_id = customer.id
            sub.plan = 'bonus'
            sub.stripe_id = 'XXXX'
            sub.active = True
            sub.created_datetime = datetime.utcnow() - timedelta(days=7)
            sub.start_datetime = datetime.utcnow() - timedelta(days=7)
            sub.end_datetime = datetime.utcnow() + timedelta(days=7)
            db.session.add(sub)
            db.session.commit()
            customer.manage_subscriptions()

            self.assertEqual(Subscription.query.get(sub.id).active, True)

            # subscription expired
            sub.active = True
            sub.end_datetime = datetime.utcnow() - timedelta(days=1)
            db.session.add(sub)
            db.session.commit()

            customer.manage_subscriptions()
            self.assertEqual(Subscription.query.get(sub.id).active, False)

            sub.active = True
            sub.plan = 'bonus'
            db.session.add(sub)
            db.session.commit()

            customer.manage_subscriptions()
            self.assertEqual(Subscription.query.get(sub.id).active, False)

    def test_deactivate(self):
        # add a member to the first customer

        with app.app_context():
            user = User.create(app, "test2@test.com", "test2")

            member = Member()
            member.customer_id = 1
            member.user_id = user.id
            member.created_datetime = datetime.utcnow()
            db.session.add(member)
            db.session.commit()

            customer = Customer.query.get(1)
            customer.deactivate()
            self.assertTrue(isinstance(user.expired, datetime))
