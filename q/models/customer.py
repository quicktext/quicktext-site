from datetime import datetime
from time import time

from sqlalchemy.dialects.postgresql import JSON

from q.q_app import app, db, stripe
from q.tasks import customerio_track


class Customer(db.Model):
    """ A customer is a paying user """

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))  # owner of the customer account
    stripe_id = db.Column(db.String(200), index=True)  # this is the stripe customer id
    created_datetime = db.Column(db.DateTime(), index=True)
    active = db.Column(db.Boolean())  # Has a valid subscription?

    subscriptions = db.relationship('Subscription', backref=db.backref('customer'))
    members = db.relationship("Member", backref=db.backref('customer'))

    def active_subscription(self):
        """ Get active subscription.

         NOTE: There can be only one active subscription at all times. """
        return Subscription.query.filter_by(customer_id=self.id).filter_by(active=True).first()

    def manage_subscriptions(self):
        """Check customer's subscription and perform actions given his status"""

        now = datetime.utcnow()

        active_subscriptions = Subscription.query.filter_by(customer_id=self.id).filter_by(active=True).all()

        # Disable any subscriptions that are no longer active (with stripe)
        for sub in active_subscriptions:
            if sub.plan == 'bonus':  # disable bonus if it has an expiration date
                if sub.end_datetime and sub.end_datetime < now:
                    sub.active = False
                    db.session.add(sub)
            else:  # a payed plan
                if not sub.stripe_id:
                    raise ValueError("Stripe ID should be set.")

                if not app.config.get('TESTING'):
                    # check on stripe if this subscription is still active
                    stripe_customer = stripe.Customer.retrieve(self.stripe_id)
                    stripe_subscription = stripe_customer.subscriptions.retrieve(sub.stripe_id)

                    # TODO(alex): Sending a reminder e-mail here?
                    customerio_track.delay(self.user_id, "subscription", status=stripe_subscription.status)
                    if stripe_subscription.status in ('canceled', 'unpaid', 'past_due'):
                        sub.active = False
                        sub.cancellation_reason += "Stripe Status: %s" % stripe_subscription.status
                        db.session.add(sub)

                    if stripe_subscription.quantity != sub.quantity:  # update if quantity changed
                        customerio_track.delay(self.user_id, "subscription", quantity=sub.quantity)
                        stripe_customer.update_subscription(
                            plan=sub.plan,
                            quantity=sub.quantity)

        db.session.commit()

        # now check if there is anything left
        if not Subscription.query.filter_by(customer_id=self.id).filter_by(active=True).count():
            customerio_track.delay(self.user_id, "subscription", expired=int(time()))
            self.deactivate()

    def deactivate(self):
        """  deactivate this customer and all it's members """

        if not self.user.expired:
            self.user.expired = datetime.utcnow()
            self.active = False
            db.session.add(self)
            db.session.add(self.user)

        for member in self.members:
            if not member.user.expired:
                member.user.expired = datetime.utcnow()
                db.session.add(member.user)
        db.session.commit()


class Member(db.Model):
    """ A customer can add members to his team """

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'))

    created_datetime = db.Column(db.DateTime(), index=True)


class Subscription(db.Model):
    """ Store subscriptions of customers.

    A periodic process should check any new subscriptions in this table and keep stripe and the db in sync

    """

    id = db.Column(db.Integer, primary_key=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'))
    stripe_id = db.Column(db.String(200), index=True)  # stripe reference

    plan = db.Column(db.String(100), index=True)  # A plan. Can be trial, bonus or real plan sku
    quantity = db.Column(db.Integer)

    active = db.Column(db.Boolean())  # - this plan is the current plan
    start_datetime = db.Column(db.DateTime(), index=True)
    # end of subscription - if a user canceled this is the date when his subscription expires
    end_datetime = db.Column(db.DateTime(), index=True)

    created_datetime = db.Column(db.DateTime(), index=True)  # when this subscription was created
    updated_datetime = db.Column(db.DateTime(), index=True)  # when this subscription was updated
    canceled_datetime = db.Column(db.DateTime(), index=True)  # user or admin canceled subscription
    cancellation_reason = db.Column(db.Text())  # motive if any


class StripeEvent(db.Model):
    """ This table holds stripe webhook data"""

    id = db.Column(db.Integer, primary_key=True)
    created_datetime = db.Column(db.DateTime(), index=True)  # when this subscription was created
    test = db.Column(db.Boolean())  # is test data
    data = db.Column(JSON)

# class Charge(db.Model):
#    """ Integration with stripe """
#
#    id = db.Column(db.Integer)
#
#class BillingAddress(db.Model):
#    id = db.Column(db.Integer, primary_key=True)
#    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
#    address = db.Column(db.Text())
#    countrycode = db.Column(db.String(2)) # iso
#    city = db.Column(db.String(250))
#    created_datetime = db.Column(db.DateTime(), index=True)
#
#    current = db.Column(db.Boolean()) # Display only this one if current
#    deleted_datetime = db.Column(db.DateTime())
#
#class Invoice(db.Model):
#    """ See integration with facturation.pro and paypal """
#
#    id = db.Column(db.Integer, primary_key=True)
#    billing_address_id = db.Column(db.Integer, db.ForeignKey('billing_address.id'))
