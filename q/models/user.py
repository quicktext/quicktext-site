from datetime import datetime, timedelta

from flask.ext.security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin
from flask.ext.security.utils import encrypt_password
from flask.ext.social import Social
from flask.ext.social.datastore import SQLAlchemyConnectionDatastore
from flask.ext.social.providers import google as google_social

from q.q_app import app, db, stripe
from q.models import customer as cmodels
from q.models import quicktext as qmodels


# Define models
roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    expired = db.Column(db.DateTime())  # this user expired, he can be a member of a customer - disable API access
    confirmed_at = db.Column(db.DateTime())
    created_datetime = db.Column(db.DateTime())

    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    connections = db.relationship('Connection',
                                  backref=db.backref('user', lazy='joined'), cascade="all")
    customer = db.relationship('Customer',
                               backref=db.backref('user'), uselist=False)
    member = db.relationship('Member',
                             backref=db.backref('user'), uselist=False)

    def info(self):
        """ Get user info """
        return dict([(row.key, row.value, ) for row in
                     UserInfo.query.filter_by(user_id=self.id).all()])
    def is_staff(self):
        """ Is the user staff or not """

        return bool(filter(lambda r: r.name == 'staff', self.roles))

    @staticmethod
    def create(context, email, password, extra=None, customer_data=None):
        ds = context.security.datastore
        user = ds.create_user(email=email, password=encrypt_password(password))
        user.created_datetime = datetime.utcnow()
        ds.commit()

        # Extra information if any
        if isinstance(extra, dict) and extra:
            if 'name' in extra:
                extra['given_name'], _, extra['family_name'] = extra['name'].partition(" ")


            exclude_fields = ('email', )
            for k, v in extra.iteritems():
                if k not in exclude_fields:
                    user_info = UserInfo()
                    user_info.user_id = user.id
                    user_info.key = str(k)
                    user_info.value = v.encode('utf-8')
                    db.session.add(user_info)
            db.session.commit()

        if customer_data is not None:  # for customers only. Members don't have subscriptions or are customers.
            stripe_id = None
            stripe_subscription_id = None
            if not app.config.get('TESTING'):

                stripe_customer = stripe.Customer.create(
                    email=email,
                    metadata={
                        'user_id': user.id,
                        'name': extra.get('name')
                    },
                )
                stripe_id = stripe_customer.id

                stripe_subscription = stripe_customer.subscriptions.create(
                    plan=customer_data['plan'],
                    card=customer_data['stripe_token'],
                    quantity=customer_data['quantity'])
                stripe_subscription_id = stripe_subscription.id

            # create customer
            customer = cmodels.Customer()
            customer.user_id = user.id
            customer.created_datetime = datetime.utcnow()
            customer.active = True
            customer.stripe_id = stripe_id
            db.session.add(customer)
            db.session.commit()

            # figure out how many days are left
            interval = app.config['PLANS'][customer_data['plan']]['interval']
            days = 365 if interval == 'year' else 30

            # create a subscription
            subscription = cmodels.Subscription()
            subscription.customer_id = customer.id
            subscription.start_datetime = datetime.utcnow()
            subscription.end_datetime = datetime.utcnow() + timedelta(days=days)
            subscription.created_datetime = datetime.utcnow()
            subscription.active = True
            subscription.plan = customer_data['plan']
            subscription.stripe_id = stripe_subscription_id
            subscription.quantity = customer_data['quantity']

            db.session.add(subscription)
            db.session.commit()

        return user


    def is_active(self):
        return self.active


    def member_users(self):
        """ Get all users that are team members, excluding yourself"""
        users = []
        # First see if the user is a customer
        if self.customer:
            members = cmodels.Member.query.filter_by(customer_id=self.customer.id).all()
            for member in members:
                users.append(member.user)
        else:
            users.append(self.member.customer.user)
            members = cmodels.Member.query.filter_by(customer_id=self.member.customer_id).filter(
                cmodels.Member.id != self.member.id).all()
            for member in members:
                users.append(member.user)

        return users


    def quicktexts(self, offset=None, limit=None):
        """ Get all users quicktexts"""
        quicktexts = set(qmodels.Quicktext.all_by(qmodels.Quicktext.user_id == self.id, offset=offset,
                                                  limit=limit))
        # then get all shared quicktexts
        quicktexts |= set(qmodels.Quicktext.all_shared(self.id, offset=offset, limit=limit))
        return quicktexts


class UserInfo(db.Model):
    """ User Info user information should be stored here """
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    key = db.Column(db.String(250))
    value = db.Column(db.Text())


class Invitation(db.Model):
    """ A user can send invitations to join gmail-quicktext.com to other
    users """

    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String(40), unique=True)  # UUID
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    email = db.Column(db.String(250))
    sent = db.Column(db.Integer, default=0)
    accepted = db.Column(db.DateTime())


class Connection(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    provider_id = db.Column(db.String(255))
    provider_user_id = db.Column(db.String(255))
    access_token = db.Column(db.String(255))
    secret = db.Column(db.String(255))

# Setup Flask-Security and Flask-Social
app.user_datastore = SQLAlchemyUserDatastore(db, User, Role)
app.security = Security(app, app.user_datastore)
app.social = Social(app, SQLAlchemyConnectionDatastore(db, Connection))

# ask for the profile and e-mail info
google_social.config['request_token_params']['scope'] = "profile email" 
