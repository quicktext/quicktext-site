from uuid import uuid4
from datetime import datetime

from q.q_app import db

QUICKTEXT_FIELDS = {'body', 'tags', 'title', 'shortcut', 'subject'}


class Quicktext(db.Model):
    """ No quicktexts are ever lost. Everytime there is a modification to a
    quicktext a new version is created in QuicktextVersion

    """

    id = db.Column(db.String(40), primary_key=True)  # UUID
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))  # original owner
    created_datetime = db.Column(db.DateTime(), index=True)
    modified_datetime = db.Column(db.DateTime())
    deleted_datetime = db.Column(db.DateTime())
    deleted = db.Column(db.Integer, default=0)  # if populated means that it was deleted

    title = db.Column(db.String, nullable=False)
    subject = db.Column(db.String, nullable=False)
    tags = db.Column(db.String, nullable=False)
    shortcut = db.Column(db.String, nullable=False)
    body = db.Column(db.String, nullable=False)

    versions = db.relationship('QuicktextVersion', backref="qt_version")

    @staticmethod
    def create(user, data):
        """ Create a new Quicktext.

        No checks should be performed in this function. Keep it simple.

        """

        db.session.begin(subtransactions=True)
        now = datetime.utcnow()

        clean_data = {k: data[k] for k in data if k in QUICKTEXT_FIELDS}

        qt_id = str(uuid4())
        try:
            qt = Quicktext(
                id=qt_id,
                user_id=user.id,
                created_datetime=now,
                modified_datetime=now,
                **clean_data
            )
            db.session.add(qt)
            db.session.commit()

            # see if the user is sharing a quicktext by default with other members
            user_info = user.info()
            share_all = user_info.get('share_all', False)
            if share_all and share_all != 'false':
                for user in user.member_users():
                    sharing = QuicktextSharing()
                    sharing.quicktext_id = qt_id
                    sharing.user_id = user.id
                    sharing.target_user_id = user.id
                    sharing.permission = 'edit'
                    sharing.created_datetime = datetime.utcnow()
                    db.session.add(sharing)
            db.session.commit()
        except:
            db.session.rollback()
            raise
        return qt

    @staticmethod
    def get(quicktext_id):
        """Get Quicktext and all it's metadata"""

        return Quicktext.query. \
            options(db.joinedload('versions')). \
            filter(Quicktext.id == quicktext_id). \
            filter(Quicktext.deleted == 0).first()

    @staticmethod
    def update(qt, user_id, data):
        """Updating quicktext means adding a new version"""
        now = datetime.utcnow()

        old_data = {k: getattr(qt, k) for k in QUICKTEXT_FIELDS}

        try:
            qt_version = QuicktextVersion(
                quicktext_id=qt.id,
                user_id=qt.user_id,
                created_datetime=qt.modified_datetime,
                **old_data
            )
            db.session.add(qt_version)

            qt.modified_datetime = now
            qt.user_id = user_id
            for k in QUICKTEXT_FIELDS:
                setattr(qt, k, data.get(k, ""))

            db.session.commit()

        except:
            db.session.rollback()
            raise
        return qt

    @staticmethod
    def all_by(filter_by, offset=None, limit=None):
        """ Get all quicktexts and their metadata by a given field (in Quicktext model) """

        if offset is None or limit is None:
            return Quicktext.query. \
                options(db.joinedload('versions')). \
                filter(Quicktext.deleted == 0). \
                filter(filter_by). \
                all()

        offset = abs(offset)
        limit = abs(limit)
        if limit > 100:  # 100 at most
            limit = 100

        return Quicktext.query. \
            join(QuicktextVersion). \
            filter(Quicktext.deleted == 0). \
            filter(filter_by). \
            offset(offset). \
            limit(limit). \
            all()

    @staticmethod
    def all_shared(user_id, offset=None, limit=None):
        """ Get all shared quicktexts with a user """

        if offset is None or limit is None:
            return Quicktext.query. \
                join(QuicktextSharing). \
                filter(Quicktext.deleted == 0). \
                filter(QuicktextSharing.deleted == 0). \
                filter(QuicktextSharing.target_user_id == user_id). \
                all()

        return Quicktext.query. \
            join(QuicktextSharing). \
            filter(Quicktext.deleted == 0). \
            filter(QuicktextSharing.deleted == 0). \
            filter(QuicktextSharing.target_user_id == user_id). \
            offset(offset). \
            limit(limit). \
            all()


class QuicktextVersion(db.Model):
    """ No quicktexts are ever lost.
    Everytime there is a modification a new row is created in this table
    """

    id = db.Column(db.Integer, primary_key=True)
    quicktext_id = db.Column(db.String(40), db.ForeignKey('quicktext.id'))  # UUID
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    created_datetime = db.Column(db.DateTime(), index=True)

    title = db.Column(db.String, nullable=False)
    subject = db.Column(db.String, nullable=False)
    tags = db.Column(db.String, nullable=False)
    shortcut = db.Column(db.String, nullable=False)
    body = db.Column(db.String, nullable=False)

    def __cmp__(self, other):
        """ Used to order by date """
        return cmp(self.created_datetime, other.created_datetime)


class QuicktextSharing(db.Model):
    """ Used to figure out which quicktexts are shared with user """

    id = db.Column(db.Integer, primary_key=True)
    quicktext_id = db.Column(db.String(40), db.ForeignKey('quicktext.id'))  # UUID
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))  # owner
    target_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))  # target
    permission = db.Column(db.String(50))  # view, edit
    created_datetime = db.Column(db.DateTime(), index=True)
    deleted = db.Column(db.Integer, default=0)  # if populated means that it was deleted


class QuicktextStats(db.Model):
    """ Contains multiple keys for the same user_id."""

    id = db.Column(db.Integer, primary_key=True)
    created_datetime = db.Column(db.DateTime(), index=True)
    # can be null if user is not auth
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user_agent = db.Column(db.Text())
    # security throttling
    ip = db.Column(db.String(15), index=True)
    key = db.Column(db.String(255), index=True)
    value = db.Column(db.Text())
