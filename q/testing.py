import unittest

from q.q_app import app, db
from q.models import user as umodels


class QTestCase(unittest.TestCase):
    """ Global Test Case - handles db creation and stuff """

    def setUp(self):
        super(QTestCase, self).setUp()

        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = app.config.get('SQLALCHEMY_DATABASE_URI_TEST')
        app.config['WTF_CSRF_ENABLED'] = False

        db.create_all()
        with app.app_context():
            umodels.User.create(app, "test@quicktext.io", "test", customer_data={
                'plan': 'yearly-eur-1', 'quantity': 2
            })

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        db.get_engine(app).dispose()
        super(QTestCase, self).tearDown()