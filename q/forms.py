from flask import current_app
from flask.ext.wtf import Form
from wtforms import TextField, PasswordField, FileField, IntegerField, ValidationError, BooleanField, HiddenField
from wtforms.validators import Length, Regexp, Optional, Required, Email


class UniqueUser(object):
    """ Unique User validator """

    def __init__(self, message="User exists"):
        self.message = message

    def __call__(self, form, field):
        if current_app.security.datastore.find_user(email=field.data):
            raise ValidationError(self.message)


validators = {
    'unique_email': [
        Required(),
        Email(),
        UniqueUser(message='Email address is associated with '
                           'an existing account')
    ],
    'email': [
        Required(),
        Email()
    ],
    'password': [
        Required(),
        Length(min=6),
        Regexp(r'[A-Za-z0-9@#$%^&+=]',
               message='Password contains invalid characters')
    ]
}


class RegisterForm(Form):
    name = TextField('Full Name', [Required()])
    email = TextField('E-mail', validators['unique_email'])
    password = PasswordField('Password', validators['password'], )
    quantity = IntegerField('Team Members', [Required()], default=2,
                            description="You can use this service alone, but it's designed to be used with a team.")
    coupon = TextField("Coupon Code")  # Optional coupon code
    stripe_token = HiddenField("Stripe Token", [Required()])


class QuicktextDataForm(Form):
    """ Validate quicktext fields """
    title = TextField('Title', [Required()])
    subject = TextField('Subject')
    shortcut = TextField('Shortcut')
    tags = TextField('Tags')
    body = TextField('Body', [Required()])


class QuicktextStatsForm(Form):
    """ Statistics that come from the users"""

    words = IntegerField("Saved Words")


class ImportForm(Form):
    import_file = FileField("Import file", [Required()])


class InvitationForm(Form):
    email0 = TextField('Email Address', [
        Email()
    ])
    email1 = TextField('Email Address', [
        Optional(),
        Email()
    ])
    email2 = TextField('Email Address', [
        Optional(),
        Email()
    ])
    email3 = TextField('Email Address', [
        Optional(),
        Email()
    ])
    email4 = TextField('Email Address', [
        Optional(),
        Email()
    ])


class AccountForm(Form):
    name = TextField("Full Name")
    email = TextField("Email", [Required(), Email()])
    password = TextField("New Password")
    share_all = BooleanField("Share All")


class MemberForm(Form):
    name = TextField("Full Name", [Required()])
    email = TextField("Email", [Required(), Email()])
    send_notification = BooleanField("Send Notification")
