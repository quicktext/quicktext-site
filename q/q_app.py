from werkzeug.contrib.profiler import ProfilerMiddleware
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.mail import Mail
from raven.contrib.flask import Sentry
from customerio import CustomerIO
from q.views.api.core import QApi
from celery import Celery
import stripe

app = Flask(__name__)
app.config.from_object('q.config')
app.config.from_envvar('Q_CONFIG', silent=True)

db = SQLAlchemy(app)
mailer = Mail(app)
sentry = Sentry(app)

api = QApi(app)
customerio = CustomerIO(app.config['CUSTOMERIO_SITE_ID'], app.config['CUSTOMERIO_API_KEY'])

stripe.api_key = app.config['STRIPE_SECRET']

def make_celery(app):
    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery

celery = make_celery(app)

# enable only when DEBUG=False
if app.config.get("DEBUG", False):
    if app.config.get("PROFILE"):
        app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions = [30])

import q.views.public
import q.views.auth
import q.views.stripe
import q.views.api.quicktext
import q.views.api.user
import q.views.api.customer
import q.views.api.staff

if __name__ == '__main__':
    app.run()
