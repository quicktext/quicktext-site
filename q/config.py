import os
import itertools

from werkzeug.exceptions import NotFound


DEBUG = True
PROFILE = False
BASE_URL = "http://localhost:5000"

# Average Words per minute (composition): http://en.wikipedia.org/wiki/Words_per_minute
AVG_WPM = 20

if os.environ.get("QUICKTEXT_PROD"):
    DEBUG = False
    BASE_URL = "https://quicktext.io"
    SENTRY_DSN = 'https://a30d038054f148618f073c8085cedf05:4b2cffe863ca4a059d92c1849aebd0df@app.getsentry.com/20325'

RAVEN_IGNORE_EXCEPTIONS = [NotFound]  # ignore not found exception
SQLALCHEMY_DATABASE_URI = 'postgres://q:q@localhost/q'
SQLALCHEMY_DATABASE_URI_TEST = 'postgres://q:q@localhost/qtest'
SQLALCHEMY_ECHO = False

CELERY_BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND='redis://localhost:6379'

SECRET_KEY = "i3993= --49 488 2828288**&^67 33993 djdjjdh"

SECURITY_PASSWORD_HASH = "sha512_crypt"
SECURITY_PASSWORD_SALT = "i3993-ds93991hh 90-((*&&^663636300 11 2334"
SECURITY_SEND_REGISTER_EMAIL = True
SECURITY_EMAIL_SENDER = "Alex Plugaru <support@quicktext.io>"
SECURITY_REGISTERABLE = True
SECURITY_RECOVERABLE = True

SECURITY_POST_LOGIN_VIEW = "/app"
SECURITY_LOGIN_USER_TEMPLATE = "public/index.html"
SECURITY_REGISTER_USER_TEMPLATE = "public/register.html"
SECURITY_EMAIL_SUBJECT_REGISTER = "Welcome to Quicktext!"

SECURITY_EMAIL_SUBJECT_PASSWORD_RESET = "Quicktext: Password reset instructions"
SECURITY_EMAIL_SUBJECT_PASSWORD_NOTICE = "Quicktext: Your password has been reset"
SECURITY_RESET_PASSWORD_TEMPLATE = "public/reset_password.html"
SECURITY_SEND_PASSWORD_CHANGE_EMAIL = False
SECURITY_SEND_PASSWORD_RESET_NOTICE_EMAIL = False

SOCIAL_GOOGLE = {
    'consumer_key': '880165099906.apps.googleusercontent.com',
    'consumer_secret': 's1k-NHxZX3iBrtdGW9RhRz7M'
}
SOCIAL_CONNECT_ALLOW_VIEW = "/app"

MAIL_SERVER = 'smtp.mandrillapp.com'
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USERNAME = 'support+mandrill@quicktext.io'
MAIL_PASSWORD = 'Nh0O09utVLHraXu0K_0C2w'

# Stripe Keys
STRIPE_SECRET = "sk_test_uNmRp7k3xZ6ZwGch72ItbSAe"
STRIPE_PUBLIC = "pk_test_CrndKgX0RqZNr0zdvWibYUMC"

if not DEBUG:
    STRIPE_SECRET = "sk_live_9kl7OEawojjTEOIwcoV3WZNs"
    STRIPE_PUBLIC = "pk_live_UiPIfyztbUFQS5ZKuKnLdNVZ"

PLANS = {
    'bonus': {
        'amount': 0,
        'interval': '',
        'name': 'Bonus',
        'currency': '',
        'currency_sign': '',
        'sku': 'bonus',
    },
    'monthly-eur-1': {
        'amount': 9.99,
        'interval': 'month',
        'name': 'Monthly',
        'currency': 'eur',
        'currency_sign': '&euro;',
        'sku': 'monthly-eur-1',
    },
    'yearly-eur-1': {
        'amount': 99.96,  # 4.99 * 12
        'interval': 'year',
        'name': 'Yearly',
        'currency': 'eur',
        'currency_sign': '&euro;',
        'sku': 'yearly-eur-1',
    },
    'monthly-usd-1': {
        'amount': 9.99,
        'interval': 'month',
        'name': 'Monthly',
        'currency': 'usd',
        'currency_sign': '$',
        'sku': 'monthly-usd-1',
    },
    'yearly-usd-1': {
        'amount': 99.96,  # 4.99 * 12
        'interval': 'year',
        'name': 'Yearly',
        'currency': 'usd',
        'currency_sign': '$',
        'sku': 'yearly-usd-1',
    },
    'monthly-gbp-1': {
        'amount': 9.99,
        'interval': 'month',
        'name': 'Monthly',
        'currency': 'gbp',
        'currency_sign': '&pound;',
        'sku': 'monthly-gbp-1',
    },
    'yearly-gbp-1': {
        'amount': 99.96,  # 4.99 * 12
        'interval': 'year',
        'name': 'Yearly',
        'currency': 'gbp',
        'currency_sign': '&pound;',
        'sku': 'yearly-gpb-1',
    },
}

ACTIVE_PLANS_BY_CURRENCY = {
    'eur': [PLANS['monthly-eur-1'], PLANS['yearly-eur-1']],
    'usd': [PLANS['monthly-usd-1'], PLANS['yearly-usd-1']],
    'gbp': [PLANS['monthly-gbp-1'], PLANS['yearly-gbp-1']],
}

ACTIVE_PLANS = list(itertools.chain(*ACTIVE_PLANS_BY_CURRENCY.values()))

HTTP_BASIC_AUTH_REALM = "User authentication required. Use your quicktext.io credentials."

CUSTOMERIO_SITE_ID = "16f89f0b9669721e4f06"
CUSTOMERIO_API_KEY = "1e4449b849d8e276f564"

AWS_BACKUP_ACCESS_KEY_ID = "AKIAJFY2YZR5EXT5KNYQ"
AWS_BACKUP_SECRET_ACCESS_KEY = "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"
WALE_S3_PREFIX = "s3://quicktext-postgresql-backup/current"
