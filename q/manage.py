import logging

from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
from q.q_app import app, db
from q.models import customer as cmodels

manager = Manager(app)
migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)
logger = logging.getLogger(__file__)


@manager.command
def handle_invitations():
    """ Send invitations """


@manager.command
def stripe():
    """ Update Stripe data both on stripe and locally """

    import stripe

    # Setup plans

    #if app.config['DEBUG']:
    #    # delete all plans when debugging
    #    for plan in stripe.Plan.all()['data']:
    #        plan.delete()

    existing_plans = [p.id for p in stripe.Plan.all()['data']]
    for sku, plan in app.config['PLANS'].items():
        if sku not in existing_plans and sku not in ('bonus'):
            stripe.Plan.create(
                amount=int(plan['amount'] * 100),  # it needs to be in cents
                interval=plan['interval'],
                name=plan['name'],
                currency=plan['currency'],
                id=sku)

    # Setup customers
    for customer in cmodels.Customer.query.all():
        if not customer.stripe_id:
            # create customers and populate customers table
            stripe_customer = stripe.Customer.create(
                email=customer.user.email,
                metadata={
                    'user_id': customer.user_id,
                },
            )
            customer.stripe_id = stripe_customer.id
            db.session.add(customer)

            logger.info("Added customer: %s" % stripe_customer.id)
        else:
            customer.manage_subscriptions()

    db.session.commit()


if __name__ == "__main__":
    manager.run()
