""" Authenticated users views """

from flask import render_template
from flask.ext.security import current_user, login_required, login_user

from q.q_app import app
from q.models import user as umodels
from q.tasks import customerio_track
from q.views.api.utils import is_staff

@app.route('/app', methods=['GET', 'POST'])
@login_required
def auth_index():
    """ Base auth index """
    customerio_track.delay(customer_id=current_user.id, name="visit")
    return render_template("auth/base.html")


@is_staff()
@login_required
@app.route('/app/staff/impersonate/<user_id>', methods=['GET'])
def impersonate(user_id):
    """Get all users information"""
    user = umodels.User.query.get_or_404(user_id)
    login_user(user)
    return '', 200
