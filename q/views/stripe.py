""" Stripe webhook handlers """
from datetime import datetime
from q.q_app import app, db
from flask import request
from q.models import customer as cmodels

@app.route('/stripe/webhook-test', methods=["POST"])
def webhook_test():
    """ This is stuff coming the test webhook """

    if not request.json:
        return "", 200

    event = cmodels.StripeEvent()
    event.created_datetime = datetime.utcnow()
    event.test = True
    event.data = request.json
    db.session.add(event)
    db.session.commit()
    return "", 200


@app.route('/stripe/webhook', methods=["POST"])
def webhook():
    """ Event data coming from production webhook"""

    if not request.json:
        return "", 200

    event = cmodels.StripeEvent()
    event.created_datetime = datetime.utcnow()
    event.test = False
    event.data = request.json
    db.session.add(event)
    db.session.commit()
    return "", 200
