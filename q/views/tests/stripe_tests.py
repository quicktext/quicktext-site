import json

from q.testing import QTestCase
from q.q_app import app
from q.models import customer as cmodels


class StripeTestCase(QTestCase):
    def setUp(self):
        super(StripeTestCase, self).setUp()
        self.c = app.test_client()

    def _webhook(self, url, test):
        # only works with post
        res = self.c.get(url)
        self.assertEqual(res.status_code, 405)
        res = self.c.post(url, data=json.dumps({}), content_type='application/json')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(cmodels.StripeEvent.query.count(), 0)

        data = {'hello': 'world'}
        res = self.c.post(url, data=json.dumps(data), content_type='application/json')
        self.assertEqual(res.status_code, 200)
        events = cmodels.StripeEvent.query.all()
        self.assertEqual(len(events), 1)
        event = events[0]
        self.assertEqual(event.test, test)
        self.assertEqual(event.data, data)

    def test_webhook_test(self):
        self._webhook('/stripe/webhook-test', True)


    def test_webhook_prod(self):
        self._webhook('/stripe/webhook', False)


