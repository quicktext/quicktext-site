import unittest

from q.utils import divide_time


class UtilsTestCase(unittest.TestCase):

    def test_divide_time(self):

        self.assertEqual(divide_time(0), {
            'seconds': 0,
            'minutes': 0,
            'hours': 0,
            'days': 0,
            'years': 0
        })

        self.assertEqual(divide_time(50), {
            'seconds': 50,
            'minutes': 0,
            'hours': 0,
            'days': 0,
            'years': 0
        })


        self.assertEqual(divide_time(100), {
            'seconds': 40,
            'minutes': 1,
            'hours': 0,
            'days': 0,
            'years': 0
        })

        self.assertEqual(divide_time(1000), {
            'seconds': 40,
            'minutes': 16,
            'hours': 0,
            'days': 0,
            'years': 0
        })

        self.assertEqual(divide_time(10000), {
            'seconds': 40,
            'minutes': 46,
            'hours': 2,
            'days': 0,
            'years': 0
        })

        self.assertEqual(divide_time(100000), {
            'seconds': 40,
            'minutes': 46,
            'hours': 3,
            'days': 1,
            'years': 0
        })

        self.assertEqual(divide_time(1000000), {
            'seconds': 40,
            'minutes': 46,
            'hours': 13,
            'days': 11,
            'years': 0
        })

        self.assertEqual(divide_time(100000000), {
            'seconds': 40,
            'minutes': 46,
            'hours': 9,
            'days': 2,
            'years': 3
        })
