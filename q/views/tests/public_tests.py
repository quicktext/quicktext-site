import urllib

from q.testing import QTestCase
from q.q_app import app
from q.models import user as umodels


class QAppTestCase(QTestCase):
    def setUp(self):
        super(QAppTestCase, self).setUp()
        self.c = app.test_client()

    def test_index(self):
        res = self.c.get('/')
        self.assertEqual(res.status_code, 200)

    def test_signup(self):
        res = self.c.get('/signup')
        self.assertEqual(res.status_code, 404)

        signup_url = '/signup/yearly-eur-1'
        res = self.c.get(signup_url)
        self.assertEqual(res.status_code, 200)

        # register a new user
        user_data = {
            'name': 'John',
            'email': 'test+new@quicktext.io',
            'password': 'test123',
            'stripe_token': "XXXX",
            'quantity': 2
        }
        res = self.c.post(signup_url, data=user_data,
                          follow_redirects=False)
        self.assertEqual(res.status_code, 302)
        self.assertTrue('/app' in res.location)

        # see if the user is actually in the database
        user = umodels.User.query.filter_by(email=user_data['email']).first()
        self.assertEqual(user.email, user_data['email'])

        self.c = app.test_client()
        # try to register an existing user (same data)
        res = self.c.post(signup_url, data=user_data,
                          follow_redirects=False)

        self.assertEqual(res.status_code, 200)
        self.assertTrue('Email address is associated with an existing account' in res.data)
        self.assertEqual(len(umodels.User.query.filter_by(email=user_data['email']).all()), 1)

    def test_signup_validation(self):
        """ Test that the registration validation actually works"""
        signup_url = '/signup/monthly-eur-1'

        # no data
        user_data = {}
        res = self.c.post(signup_url, data=user_data)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data.count("This field is required."), 3)

        # invalid e-mail
        user_data = {'email': 'invalid'}
        res = self.c.post(signup_url, data=user_data)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data.count("Invalid email address"), 1)

        # password to short
        user_data = {'email': 'test+newuser@quicktext.io', 'password': '1', 'name': "John", 'quantity': 1}
        res = self.c.post(signup_url, data=user_data)

        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.data.count("Invalid email address"), 0)
        self.assertEqual(res.data.count("Field must be at least 6 characters long."), 1)

        # make sure we don't have any users created
        self.assertEqual(len(umodels.User.query.filter_by(email=user_data['email']).all()), 0)

    def test_signin(self):
        res = self.c.get('/signin')
        self.assertEqual(res.status_code, 302)

        # register a new user
        user_data = {
            'name': 'John',
            'email': 'test+new@quicktext.io',
            'password': 'test123',
            'stripe_token': "XXXX",
            'quantity': 2
        }
        signup_url = '/signup/monthly-eur-1'
        self.c.post(signup_url, data=user_data)

        self.c = app.test_client()
        # password not good
        res = self.c.post('/signin', data={'email': user_data['email'], 'password': '23232'},
                          follow_redirects=False)
        self.assertTrue("Login failed" in urllib.unquote(res.location))

        # finally signin
        res = self.c.post('/signin', data=user_data, follow_redirects=True)
        self.assertTrue('logout' in res.data)

    def test_privacy(self):
        res = self.c.get("/privacy")
        self.assertEquals(res.status_code, 200)
        self.assertTrue(res.data, "Privacy")

    def test_notfound(self):
        res = self.c.get('/notfound')
        self.assertEqual(res.status_code, 404)
        self.assertTrue('Not found' in res.data)
