from flask.ext.restful import reqparse, Resource

from q.q_app import api

from q.models import user as umodels
from .utils import authenticate, is_staff

parser = reqparse.RequestParser()


class StaffResource(Resource):
    decorators = [authenticate(), is_staff()]


class UserResource(StaffResource):
    def get(self):
        """Get all users information"""
        users = umodels.User.query.order_by(umodels.User.created_datetime.desc()).all()
        ret = []
        for user in users:
            ret.append({
                'id': user.id,
                'email': user.email,
                'created_datetime': user.created_datetime.isoformat(),
                'active': user.active,
                'roles': [r.name for r in user.roles],
                'is_customer': bool(user.customer),
                'info': user.info()
            })
        return ret, 200


api.add_resource(UserResource, '/api/1/staff/users')
