from datetime import datetime, timedelta
from sqlalchemy import Integer, func, cast

from flask import request, json, jsonify
from flask.ext.security import current_user
from flask.ext.restful import reqparse, abort, Resource

from q.q_app import api, db, app
from q import forms, tasks, config
from q.models import quicktext as qmodels
from q.models import user as umodels
from q.utils import parse_thunderbird_quicktext, divide_time, reduce_numbers
from .utils import authenticate


parser = reqparse.RequestParser()
parser.add_argument('all', type=str)  # get all versions if this is true
parser.add_argument('version', type=int)  # get a specific version
parser.add_argument('offset', type=int)  # pagination
parser.add_argument('limit', type=int)  # pagination


def serialize_quicktext_version(row):
    if isinstance(row, qmodels.Quicktext):
        time = row.modified_datetime
    else:
        time = row.created_datetime

    rv = {
        'created_datetime': time.isoformat(),
    }
    for key in qmodels.QUICKTEXT_FIELDS:
        rv[key] = getattr(row, key)
    return rv


def serialize_quicktext(qt, all_versions=False):
    versions = [serialize_quicktext_version(qt)]
    if all_versions:
        for v in qt.versions:
            versions.append(serialize_quicktext_version(v))

    return {
        'id': qt.id,
        'created_datetime': qt.created_datetime.isoformat(),
        'versions': versions,
    }


class QuicktextResource(Resource):
    decorators = [authenticate()]


class QuicktextItem(QuicktextResource):
    def get(self, quicktext_id):
        """Get a quicktext. With all versions"""

        qt = qmodels.Quicktext.get(quicktext_id)
        if not qt:
            abort(404)
        args = parser.parse_args()
        return serialize_quicktext(qt, args.all), 200

    def delete(self, quicktext_id):
        qt = qmodels.Quicktext.get(quicktext_id)
        if not qt:
            abort(404)

        qt.deleted = 1
        qt.deleted_datetime = datetime.utcnow()
        db.session.add(qt)
        db.session.commit()
        return '', 200

    def put(self, quicktext_id):
        """ Update quicktext """
        qt = qmodels.Quicktext.get(quicktext_id)
        if not qt:
            abort(404)
        form = forms.QuicktextDataForm(csrf_enabled=False)
        if form.validate():
            qt = qmodels.Quicktext.update(qt, current_user.id, form.data)
            return serialize_quicktext(qt), 201
        return form.errors, 400


class QuicktextList(QuicktextResource):
    def get(self):
        """ Get all accessible quicktexts for the auth user.

        """

        args = parser.parse_args()

        quicktexts = current_user.quicktexts(args.offset, args.limit)

        data = []
        for qt in quicktexts:
            data.append(serialize_quicktext(qt, args.all))
        return data

    def post(self):
        """ Create a new quicktext.

        """
        form = forms.QuicktextDataForm(csrf_enabled=False)
        if form.validate():
            qt = qmodels.Quicktext.create(current_user, form.data)
            return serialize_quicktext(qt), 201
        return form.errors, 400

    def put(self):
        """ Perform operations on a bunch of quicktexts (delete, update, etc..) """

        quicktext_ids = request.json.get('quicktext_ids', None)
        action = request.json.get('action', None)

        if action == 'delete':
            return self._delete(quicktext_ids)
        else:
            return "Please provide an action", 400

        return "", 200

    def _delete(self, quicktext_ids):
        """ Delete multiple quicktexts """

        #TODO: Check that the user has the right to delete the quicktexts
        quicktexts = qmodels.Quicktext.query.filter(qmodels.Quicktext.id.in_(quicktext_ids)).all()
        for qt in quicktexts:
            qt.deleted = 1
            qt.deleted_datetime = datetime.utcnow()
            db.session.add(qt)
        db.session.commit()


class QuicktextListInfo(QuicktextResource):
    def get(self):
        """"""
        # TODO: shared quicktexts?
        return {
            "own_quicktexts": qmodels.Quicktext.filter_by(qmodels.Quicktext.user_id == current_user.id).count(),
        }


class QuicktextImport(QuicktextResource):
    def _parse_upload(self, upload):
        try:
            data = json.load(upload)
        except ValueError:
            upload.seek(0)
        else:
            return [
                {k: q[k] for k in q if k in qmodels.QUICKTEXT_FIELDS}
                for q in data['quicktexts']
            ]

        try:
            return parse_thunderbird_quicktext(upload)
        except ValueError:
            return None


    def post(self):
        """ Import JSON or Thunderbird .xml"""
        quicktexts = self._parse_upload(request.files['file'])
        if quicktexts is None:
            return "Failed parsing file", 400

        # TODO: this could be a problem for a lot of quicktexts
        for qt in quicktexts:
            qmodels.Quicktext.create(current_user, qt)
        return "", 201


class QuicktextExport(QuicktextResource):
    def get(self):
        args = parser.parse_args()
        quicktexts = current_user.quicktexts()

        def dump(qt):
            data = serialize_quicktext(qt, args.all)
            data.update(data.pop('versions')[0])
            return data

        return jsonify(quicktexts=[dump(qt) for qt in quicktexts])


class QuicktextStats(Resource):
    decorators = [authenticate(required=False)]

    def get(self):
        """Get stats of quicktexts for the current user

        Only supports 'words' at the moment

        """

        if current_user.is_anonymous():
            abort(404)

        start_date = datetime.strptime(request.args.get('startDate'), '%Y-%m-%d')
        end_date = datetime.strptime(request.args.get('endDate'), '%Y-%m-%d')

        def daterange(start_date, end_date):
            for n in range(int((end_date + timedelta(days=1) - start_date).days)):
                yield start_date + timedelta(n)


        wpm = app.config.get("AVG_WPM")
        word_count = db.session.query(
            func.sum(cast(qmodels.QuicktextStats.value, Integer)).label('words')). \
            filter_by(user_id=current_user.id). \
            filter(qmodels.QuicktextStats.key == 'words'). \
            first()
        word_count = word_count[0]
        if not word_count:
            saved_time = 0
        else:
            saved_time = round(word_count / wpm)

        data = {
            'data': {
                'count': reduce_numbers(word_count),
                'times': divide_time(saved_time),
                'wpm': wpm
            },
            'words': {
                'account': {},
                'team': {}
            }
        }

        # initialize the data first
        for single_date in daterange(start_date, end_date):
            single_date_str = single_date.strftime("%Y-%m-%d")
            data['words']['account'][single_date_str] = 0
            data['words']['team'][single_date_str] = 0

        account_stats = qmodels.QuicktextStats.query. \
            filter_by(user_id=current_user.id). \
            filter(qmodels.QuicktextStats.created_datetime > start_date). \
            filter(qmodels.QuicktextStats.created_datetime <= end_date). \
            all()

        for stat in account_stats:
            stat_date = stat.created_datetime.strftime("%Y-%m-%d")
            if stat.key == 'words':
                data['words']['account'][stat_date] += int(stat.value)

        member_ids = [m.id for m in current_user.member_users()]
        if member_ids:
            member_stats = qmodels.QuicktextStats.query. \
                filter(umodels.User.id.in_(member_ids)). \
                filter(qmodels.QuicktextStats.created_datetime > start_date). \
                filter(qmodels.QuicktextStats.created_datetime <= end_date). \
                all()

            for stat in member_stats:
                stat_date = stat.created_datetime.strftime("%Y-%m-%d")
                if stat.key == 'words':
                    data['words']['team'][stat_date] += int(stat.value)

            # finally calculate an average for all members per day
            for date, value in data['words']['team'].items():
                if value != 0:
                    data['words']['team'][date] = int(value / len(member_ids))

        return data, 200

    def post(self):
        """ Create or update stats.

        """

        form = forms.QuicktextStatsForm(csrf_enabled=False)
        if form.validate():
            for field, value in form.data.items():
                if value is not None:
                    qt_stat = qmodels.QuicktextStats()
                    if not current_user.is_anonymous():
                        qt_stat.user_id = current_user.id

                    qt_stat.ip = request.remote_addr
                    qt_stat.created_datetime = datetime.utcnow()
                    qt_stat.key = field
                    qt_stat.value = value

                    db.session.add(qt_stat)
                    db.session.commit()
            return "", 201
        return form.errors, 400


class QuicktextSharingItem(QuicktextResource):
    def get(self, quicktext_id):
        """ Get quicktext sharing data """

        # TODO: Some checking if the data of the quicktext is actually permitted to be fetched
        qt = qmodels.Quicktext.get(quicktext_id)
        if not qt:
            abort(404)

        user = umodels.User.query.get(qt.user_id)
        user_info = user.info()

        data = [{
                    "id": "",
                    "quicktext_id": qt.id,
                    "user_id": qt.user_id,
                    "target_user_id": qt.user_id,
                    "email": user.email,
                    "given_name": user_info.get("given_name", ""),
                    "family_name": user_info.get("family_name", ""),
                    "permission": "owner",
                    "created_datetime": qt.created_datetime.isoformat()
                }]

        # TODO: some sort of checking (does the user actually has the right to see info for this quicktext)
        # get all share information
        shares = qmodels.QuicktextSharing.query.filter_by(
            quicktext_id=quicktext_id).filter_by(deleted=0).all()

        for share in shares:
            user = umodels.User.query.get(share.user_id)
            user_info = user.info()

            data.append({
                "id": share.id,
                "quicktext_id": share.quicktext_id,
                "user_id": share.user_id,
                "target_user_id": share.target_user_id,
                "email": user.email,
                "given_name": user_info.get("given_name", ""),
                "family_name": user_info.get("family_name", ""),
                "permission": share.permission,
                "created_datetime": share.created_datetime.isoformat()
            })
        return data, 200

    def post(self, quicktext_id):
        """ Create a new ACL for a given quicktext """

        # TODO: at the moment allow only the original author to share a quicktext
        qt = qmodels.Quicktext.query.filter_by(id=quicktext_id).filter_by(
            user_id=current_user.id).first()

        if not qt:
            abort(404)

        target_user_id = int(request.form['user_id'])
        target_user = umodels.User.query.get(target_user_id)
        if not target_user:
            return {"error": "Unknown target user"}, 400

        # check if sharing already exists
        qt_sharing = qmodels.QuicktextSharing.query.filter_by(
            quicktext_id=quicktext_id).filter_by(
            user_id=current_user.id).filter_by(
            target_user_id=target_user_id).filter_by(
            deleted=0).first()
        if qt_sharing:
            return "", 201

        qt_sharing = qmodels.QuicktextSharing()
        qt_sharing.quicktext_id = quicktext_id
        qt_sharing.user_id = current_user.id
        qt_sharing.target_user_id = target_user_id
        qt_sharing.created_datetime = datetime.utcnow()

        db.session.add(qt_sharing)
        db.session.commit()

        return "", 201

    def delete(self, quicktext_id):
        """ Stop sharing the quicktext """

        qt = qmodels.Quicktext.get(quicktext_id)
        if not qt:
            abort(404)

        quicktext_sharing_id = int(request.args.get('quicktext_sharing_id', 0))

        if not quicktext_sharing_id:
            abort(404)

        qt_sharing = qmodels.QuicktextSharing.query.filter_by(
            id=quicktext_sharing_id).filter_by(
            user_id=current_user.id).first()

        if not qt_sharing:
            abort(404)

        # mark as deleted
        qt_sharing.deleted = 1
        db.session.add(qt_sharing)
        db.session.commit()

        return "", 200


class QuicktextSharingList(QuicktextResource):
    def post(self):
        """ Post here will be used to `GET` multiple quicktext ACL at once.

        The reason behind this is that we can't have a GET request with very long URI.
        Use `PUT` to update quicktexts ACL

        """
        quicktext_ids = request.form.getlist('quicktext_ids') or request.json.get('quicktext_ids')
        quicktexts = qmodels.Quicktext.query.filter(qmodels.Quicktext.id.in_(quicktext_ids)).all()

        if not quicktexts:
            abort(404)

        data = []
        user_data = {}

        def get_user_data(user_id):
            """Try not fetch the user data twice, instead use a dict and store previous results"""

            if user_id in user_data:
                return user_data[user_id]

            user = umodels.User.query.get(user_id)
            user_info = user.info()
            user_data[user_id] = (
                user, user_info
            )
            return user, user_info

        for qt in quicktexts:
            user, user_info = get_user_data(qt.user_id)
            data.append({
                "id": "",
                "quicktext_id": qt.id,
                "user_id": qt.user_id,
                "target_user_id": qt.user_id,
                "email": user.email,
                "given_name": user_info.get("given_name", ""),
                "family_name": user_info.get("family_name", ""),
                "permission": "owner",
                "created_datetime": qt.created_datetime.isoformat()
            })

        # Get existing sharing data
        sharing_data = qmodels.QuicktextSharing.query. \
            filter(qmodels.QuicktextSharing.quicktext_id.in_(quicktext_ids)). \
            filter_by(deleted=0). \
            filter_by(user_id=current_user.id).all()

        for share in sharing_data:
            user, user_info = get_user_data(share.target_user_id)
            data.append({
                "id": share.id,
                "quicktext_id": share.quicktext_id,
                "user_id": share.user_id,
                "target_user_id": share.target_user_id,
                "email": user.email,
                "given_name": user_info.get("given_name", ""),
                "family_name": user_info.get("family_name", ""),
                "permission": share.permission,
                "created_datetime": share.created_datetime.isoformat()
            })
        return {'acl': data}

    def put(self):
        """ Given a dict of quicktext_ids, for each quicktext_id give a user_id a specific permission """

        acl = request.json.get('acl', None)
        action = request.json.get('action', None)

        if action == 'create':
            return self._create(acl)
        elif action == 'delete':
            return self._delete(acl)
        else:
            return "Please provide an action", 400

        return "", 200

    def _create(self, acl):

        quicktext_ids = acl['quicktexts'].keys()
        quicktexts = qmodels.Quicktext.query.filter(qmodels.Quicktext.id.in_(quicktext_ids)).all()

        if len(quicktext_ids) != len(quicktexts):
            raise ValueError("not all quicktexts found")

        # get the existing sharing data for the current user
        quicktext_sharing = qmodels.QuicktextSharing.query. \
            filter(qmodels.QuicktextSharing.quicktext_id.in_(acl['quicktexts'].keys())). \
            filter_by(deleted=0). \
            filter_by(user_id=current_user.id).all()

        # group sharing data by quicktexts
        sharing_data = {}
        for share in quicktext_sharing:
            if share.quicktext_id not in sharing_data:
                sharing_data[share.quicktext_id] = {}
            sharing_data[share.quicktext_id][share.target_user_id] = share.permission


        email_user_ids = {}
        # get all user_ids given e-mails or create users if they doesn't exist
        # first collect all e-mails
        emails = set()
        for acl_item in acl['quicktexts'].values():
            map(emails.add, acl_item['emails'].split(','))

        for email in emails:
            # get an existing user
            user = umodels.User.query.filter_by(email=email).first()
            if user:
                email_user_ids[email] = user.id
            else:  # don't create new users for now
                pass

        mails = {}
        for quicktext_id, acls in acl['quicktexts'].items():
            permission = acls['permission']
            for email in acls['emails'].split(','):
                user_id = email_user_ids.get(email)
                if user_id is None:
                    continue
                if user_id == current_user.id:
                    return "You can't share quicktexts with yourself", 400

                if (quicktext_id in sharing_data and
                            user_id in sharing_data[quicktext_id] and
                            sharing_data[quicktext_id][user_id] == permission):
                    continue  # nothing to do here, the user has the same permission as before

                qts = set(filter(lambda q: q.id == quicktext_id, quicktexts))
                if email not in mails:
                    mails[email] = qts
                mails[email] |= qts

                # delete any existing sharing with this user on this quicktext and create a new one
                old_sharing = qmodels.QuicktextSharing.query. \
                    filter_by(quicktext_id=quicktext_id). \
                    filter_by(user_id=current_user.id). \
                    filter_by(target_user_id=user_id).all()

                for share in old_sharing:
                    share.deleted = 1
                    db.session.add(share)

                # Create a new share
                sharing = qmodels.QuicktextSharing()
                sharing.quicktext_id = quicktext_id
                sharing.user_id = current_user.id
                sharing.target_user_id = user_id
                sharing.created_datetime = datetime.utcnow()

                # TODO: Validate the permission!
                sharing.permission = 'edit'
                db.session.add(sharing)
        db.session.commit()

        for address, quicktexts_email in mails.items():
            tasks.send_mail("New quicktext" + "s" if len(quicktexts_email) > 1 else "",
                            config.SECURITY_EMAIL_SENDER,
                            [address],
                            "emails/share_notification.txt",
                            {'quicktexts': quicktexts_email,
                             'customer_name': current_user.info().get('name', ''),
                             'message': acl['message']
                            })

        return "", 201


    def _delete(self, acl):
        """ Given a list of quicktexts and a user id, delete it's sharing """

        # TODO: First check if the user actually has the right permission to change the ACL
        quicktext_ids = acl['quicktext_ids']
        user_id = acl['user_id']

        sharing_data = qmodels.QuicktextSharing.query. \
            filter(qmodels.QuicktextSharing.quicktext_id.in_(quicktext_ids)). \
            filter_by(target_user_id=user_id). \
            all()

        for share in sharing_data:
            share.deleted = 1
            db.session.add(share)
        db.session.commit()

        return "", 200

# Routing
api.add_resource(QuicktextList, '/api/1/quicktexts')
api.add_resource(QuicktextListInfo, '/api/1/quicktexts/info')
api.add_resource(QuicktextItem, '/api/1/quicktexts/<string:quicktext_id>')
api.add_resource(QuicktextImport, '/api/1/quicktexts/import')
api.add_resource(QuicktextExport, '/api/1/quicktexts/export/quicktexts.json')
api.add_resource(QuicktextStats, '/api/1/stats')
api.add_resource(QuicktextSharingList, '/api/1/share')
api.add_resource(QuicktextSharingItem, '/api/1/share/<string:quicktext_id>')
