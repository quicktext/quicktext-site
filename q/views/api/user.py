from flask.ext.security import current_user
from flask.ext.security.utils import encrypt_password


from flask.ext.restful import reqparse, Resource

from q.q_app import api, db
from q.forms import AccountForm
from q.models import user as umodels
from .utils import authenticate

parser = reqparse.RequestParser()


class UResource(Resource):
    decorators = [authenticate()]


class AccountResource(UResource):
    def get(self):
        """Get all account information"""
        allowed_keys = ("name", "locale", "gender", "share_all")
        user_info = current_user.info()
        info = dict([(key, user_info.get(key, "")) for key in allowed_keys])

        return {
            'id': current_user.id,
            'email': current_user.email,
            'is_customer': bool(current_user.customer),
            'is_staff': current_user.is_staff(),
            'created_datetime': current_user.created_datetime,
            'info': info
        }, 200

    def put(self):
        """ Update account"""

        def replace_info(key, value):
            """ Create or Update UserInfo """
            
            info = umodels.UserInfo.query.filter_by(user_id=current_user.id).filter_by(key=key).first()
            if info:
                info.value = value
            else:
                info = umodels.UserInfo()
                info.key = key
                info.value = value
                info.user_id = current_user.id

            db.session.add(info)

        form = AccountForm(csrf_enabled=False)
        if form.validate():
            if form.data['password']:
                current_user.password = encrypt_password(form.data['password'])
            current_user.email = form.data['email']
            db.session.add(current_user)

            #update user info
            replace_info('name', form.data['name'])
            replace_info('share_all', form.data['share_all'])

            db.session.commit()

        return "", 201


class LoginInfoResource(Resource):

    decorators = [authenticate(required=False)]

    def get(self):
        """ Is the user logged it or not? """
        return {
            'is_loggedin': not current_user.is_anonymous()
        }

# Routing
api.add_resource(AccountResource, '/api/1/account')
api.add_resource(LoginInfoResource, '/api/1/login-info')
