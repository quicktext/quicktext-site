import json
from datetime import datetime, timedelta
from StringIO import StringIO
import os
from sqlalchemy.orm import aliased

from q.q_app import app, db
from q.models import user as umodels
from q.models import quicktext as qmodels
from .core import ApiTestCase


class QuicktextListTestCase(ApiTestCase):
    def test_quicktexts_auth(self):
        r = self.get("quicktexts")
        self.assertEqual(r.status_code, 401)  # unauthorized

        r = self.post("quicktexts", data={})
        self.assertEqual(r.status_code, 401)  # unauthorized

    def test_get_quicktexts(self):
        """ Try getting quicktexts """

        self.login("test@quicktext.io", "test")
        r = self.get("quicktexts")
        self.assertEqual(r.status_code, 200)
        # no quicktexts yet, should be empty
        self.assertEqual(json.loads(r.data), [])
        user_id = 1
        quicktext = {
            "user": umodels.User.query.get(user_id),
            "data": {
                "title": 'test',
                "subject": 'subject_test',
                "tags": 'tag1, tag2',
                "shortcut": 'cut',
                "body": "Hello World"
            }
        }

        qmodels.Quicktext.create(**quicktext)
        r = self.get("quicktexts")
        self.assertEqual(r.status_code, 200)
        qt = json.loads(r.data)
        self.assertEqual(len(qt), 1)
        for key, val in quicktext['data'].items():
            self.assertEqual(val, qt[0]['versions'][0][key])

        # check if we have created_datetime field
        self.assertTrue('created_datetime' in qt[0])
        self.assertTrue('created_datetime' in qt[0]['versions'][0])

    def test_post_quicktexts(self):
        """ Create new quicktext """

        self.login("test@quicktext.io", "test")
        r = self.post("quicktexts", data={})
        self.assertEqual(r.status_code, 400)
        self.assertEqual(json.loads(r.data)['body'][0],
                         'This field is required.')

        quicktext = {
            "title": 'test',
            "subject": 'subject_test',
            "tags": 'tag1, tag2',
            "shortcut": 'cut',
            "body": "Hello World"
        }
        r = self.post("quicktexts", data=quicktext)
        self.assertEqual(r.status_code, 201)
        qt = json.loads(r.data)
        for key, val in quicktext.items():
            self.assertEqual(val, qt['versions'][0][key])

        # check if we have created_datetime field
        self.assertTrue('created_datetime' in qt)
        self.assertTrue('created_datetime' in qt['versions'][0])


class QuicktextItemTestCase(ApiTestCase):
    def test_get_quicktext(self):
        """ Get a quicktext """

        self.login("test@quicktext.io", "test")

        # should fail
        r = self.get('quicktexts/XXX')
        self.assertEqual(r.status_code, 404)

        user_id = 1
        original_quicktext = {
            "user": umodels.User.query.get(user_id),
            "data": {
                "title": 'test',
                "subject": 'subject_test',
                "tags": 'tag1, tag2',
                "shortcut": 'cut',
                "body": "Hello World"
            }
        }

        qt = qmodels.Quicktext.create(**original_quicktext)
        # should fail
        r = self.get('quicktexts/%s' % qt.id)
        self.assertEqual(r.status_code, 200)

        qt = json.loads(r.data)
        for key, val in original_quicktext['data'].items():
            self.assertEqual(val, qt['versions'][0][key])

    def test_put_quicktext(self):
        """ Update a quicktext """

        self.login("test@quicktext.io", "test")
        r = self.put('quicktexts/XXX', data={})
        self.assertEqual(r.status_code, 404)

        user_id = 1
        original_quicktext = {
            "user": umodels.User.query.get(user_id),
            "data": {
                "title": 'test',
                "subject": 'subject_test',
                "tags": 'tag1, tag2',
                "shortcut": 'cut',
                "body": "Hello World"
            }
        }

        qt = qmodels.Quicktext.create(**original_quicktext)
        # should fail
        r = self.put('quicktexts/%s' % qt.id, data={})
        self.assertEqual(r.status_code, 400)

        updated_quicktext = {
            "title": 'updated_test',
            "subject": 'updated_subject_test',
            "tags": 'updated_tag1, tag2',
            "shortcut": 'updated_cut',
            "body": "updated_Hello World"
        }

        r = self.put('quicktexts/%s' % qt.id, data=updated_quicktext)
        self.assertEqual(r.status_code, 201)

        r = self.get('quicktexts/%s?all=true' % qt.id)
        self.assertEqual(r.status_code, 200)

        qt = json.loads(r.data)
        self.assertEqual(len(qt['versions']), 2)

        for key, val in updated_quicktext.items():
            self.assertEqual(val, qt['versions'][0][key])

        for key, val in original_quicktext['data'].items():
            self.assertEqual(val, qt['versions'][1][key])

    def test_delete_quicktext(self):
        """ Delete a quicktext """

        self.login("test@quicktext.io", "test")

        # should fail
        r = self.delete('quicktexts/XXX')
        self.assertEqual(r.status_code, 404)

        user_id = 1
        original_quicktext = {
            "user": umodels.User.query.get(user_id),
            "data": {
                "title": 'test',
                "subject": 'subject_test',
                "tags": 'tag1, tag2',
                "shortcut": 'cut',
                "body": "Hello World"
            }
        }

        qt = qmodels.Quicktext.create(**original_quicktext)
        # should fail
        r = self.delete('quicktexts/%s' % qt.id)
        self.assertEqual(r.status_code, 200)

        qt = qmodels.Quicktext.query.first()
        self.assertEqual(qt.deleted, 1)
        self.assertNotEqual(qt.deleted_datetime, None)

    def test_import_quicktexts(self):

        self.login("test@quicktext.io", "test")

        r = self.post("quicktexts/import", data={
            'file': (StringIO("XXX"), 'file')
        })
        self.assertEquals(r.status_code, 400)
        self.assertTrue("Failed parsing file" in json.loads(r.data))

        thunderbird_xml = os.path.join(os.path.dirname(__file__), "fixtures/quicktext-francois-2013-03-29.xml")
        r = self.post("quicktexts/import", data={
            'file': (open(thunderbird_xml), 'file')
        })
        self.assertEquals(r.status_code, 201)
        self.assertEquals(json.loads(r.data), "")

        #TODO: add more extensive tests for importing when the necessary transformations are done
        #      Ex: If [en] found in the Title, remove it and add it as a tag

        txtmap = {
            v.shortcut: {
                'tags': [t.strip() for t in v.tags.split(',')],
                'body': v.body,
            }
            for v in qmodels.Quicktext.query
        }

        self.assertEqual(txtmap['newcorrect']['tags'], ['SUPPORT'])
        self.assertEqual(txtmap['frwreceive']['tags'], ['FR', 'SALES'])
        self.assertEqual(txtmap['audioserver']['tags'], ['SALES', 'TECH'])
        self.assertIn("{{to.0.email}}", txtmap['frwmail']['body'])

    def test_export_json(self):

        self.login("test@quicktext.io", "test")
        r = self.get("quicktexts/export/quicktexts.json")
        self.assertEquals(r.status_code, 200)
        dump = json.loads(r.data)
        self.assertEqual(dump['quicktexts'], [])

        qmodels.Quicktext.create(
            user=umodels.User.query.get(1),
            data={
                "title": 'test',
                "subject": 'subject_test',
                "tags": 'tag1, tag2',
                "shortcut": 'cut',
                "body": "Hello World"
            },
        )

        dump2 = json.loads(self.get("quicktexts/export/quicktexts.json").data)
        data = dump2['quicktexts'][0]
        self.assertEqual(data['title'], 'test')
        self.assertEqual(data['subject'], 'subject_test')
        self.assertEqual(data['tags'], 'tag1, tag2')
        self.assertEqual(data['shortcut'], 'cut')
        self.assertEqual(data['body'], "Hello World")

    def test_import_json(self):
        test_data = {
            'body': 'Hello World',
            'tags': 'tag1, tag2',
            'title': 'test',
            'shortcut': 'cut',
            'subject': 'subject_test',
        }
        dump_data = dict(
            test_data,
            created_datetime='2010-01-01T12:00:00.000000',
        )

        data_json = json.dumps({'quicktexts': [dump_data]})

        self.login("test@quicktext.io", "test")

        r = self.post("quicktexts/import", data={
            'file': (StringIO(data_json), 'file')
        })
        self.assertEquals(r.status_code, 201)

        [qt] = qmodels.Quicktext.query
        data = {key: getattr(qt, key) for key in test_data.keys()}

        self.assertEqual(data, test_data)


class QuicktextStatsTestCase(ApiTestCase):
    def test_update_stats(self):
        # try to update an unknown key
        self.login("test@quicktext.io", "test")
        r = self.post("stats", data={'x': 'y'})

        # even if the field doesn't exist it should still return a success status_code
        self.assertEqual(r.status_code, 201)

        # try to update words
        r = self.post("stats", data={'words': 'y'})
        self.assertEqual(r.status_code, 400)
        self.assertEqual(json.loads(r.data)['words'], ['Not a valid integer value', ])

        r = self.post("stats", data={'words': 100})
        self.assertEqual(r.status_code, 201)

        stats = qmodels.QuicktextStats.query.all()
        self.assertEqual(len(stats), 1)
        self.assertEqual(stats[0].value, '100')

        r = self.post("stats", data={'words': 100})
        self.assertEqual(r.status_code, 201)

        stats = qmodels.QuicktextStats.query.all()
        self.assertEqual(len(stats), 2)  # two entries
        self.assertEqual(stats[0].value, '100')
        self.assertEqual(stats[1].value, '100')

    def test_get_stats(self):
        self.login("test@quicktext.io", "test")
        now = datetime.utcnow()
        today = now.strftime('%Y-%m-%d')
        last_week = (now - timedelta(days=7)).strftime('%Y-%m-%d')

        r = self.get("stats?startDate=%s&endDate=%s" % (last_week, today))
        self.assertEqual(r.status_code, 200)
        rdata = json.loads(r.data)
        self.assertEqual(rdata['data']['count'], "0")
        self.assertTrue('wpm' in rdata['data'])
        self.assertTrue('words' in rdata)
        self.assertEquals(rdata['words'].keys(), ['account', 'team'])

        # add some stats
        r = self.post("stats", data={'words': 100})
        self.assertEqual(r.status_code, 201)

        r = self.get("stats?startDate=%s&endDate=%s" % (last_week, today))
        self.assertEqual(r.status_code, 200)
        rdata = json.loads(r.data)
        self.assertEqual(rdata['data']['count'], 100)

    def test_get_anon_stats(self):
        """ Deny anonymous users to get stats """
        r = self.get("stats")
        self.assertEqual(r.status_code, 404)

    def test_post_anon_stats(self):
        """ Allow anonymous users to post stats """

        r = self.post("stats", data={'words': 100})
        self.assertEqual(r.status_code, 201)

        stats = qmodels.QuicktextStats.query.all()
        self.assertEqual(len(stats), 1)
        self.assertEqual(stats[0].value, '100')


class QuicktextSharingTestCase(ApiTestCase):
    def test_simple_share(self):
        """ A user creates a quick test and shares it with someone else """

        user_id = 1
        target_user_id = 2

        self.login("test@quicktext.io", "test")
        r = self.post('share/XXX', data={'user_id': target_user_id})
        self.assertEqual(r.status_code, 404)

        quicktext = {
            "user": umodels.User.query.get(user_id),
            "data": {
                "title": 'test',
                "subject": 'subject_test',
                "tags": 'tag1, tag2',
                "shortcut": 'cut',
                "body": "Hello World"
            }
        }

        qt = qmodels.Quicktext.create(**quicktext)
        r = self.post('share/%s' % qt.id, data={'user_id': target_user_id})
        self.assertEqual(r.status_code, 400)
        self.assertEqual(json.loads(r.data), {u'error': u'Unknown target user'})

        with app.app_context():
            umodels.User.create(app, "test2@test.com", "test")

        r = self.post('share/%s' % qt.id, data={'user_id': target_user_id})
        self.assertEqual(r.status_code, 201)

        sharing = qmodels.QuicktextSharing.query.all()
        self.assertEqual(len(sharing), 1)
        self.assertEqual(sharing[0].user_id, user_id)
        self.assertEqual(sharing[0].target_user_id, target_user_id)

    def test_get_share(self):
        """ Given a quicktext, get it's sharing data """

        user_id = 1
        with app.app_context():
            target_user = umodels.User.create(app, 'test2@quicktext.io', '1111')
            target_user_id = target_user.id

        self.login("test@quicktext.io", "test")

        r = self.get('share/XXX')
        self.assertEqual(r.status_code, 404)

        quicktext = {
            "user": umodels.User.query.get(user_id),
            "data": {
                "title": 'test',
                "subject": 'subject_test',
                "tags": 'tag1, tag2',
                "shortcut": 'cut',
                "body": "Hello World"
            }
        }

        qt = qmodels.Quicktext.create(**quicktext)

        # no sharing yet
        r = self.get('share/%s' % qt.id)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(len(json.loads(r.data)), 1)

        qt_sharing = qmodels.QuicktextSharing()
        qt_sharing.quicktext_id = qt.id
        qt_sharing.user_id = user_id
        qt_sharing.target_user_id = target_user_id
        qt_sharing.created_datetime = datetime.utcnow()

        db.session.add(qt_sharing)
        db.session.commit()

        r = self.get('share/%s' % qt.id)
        self.assertEqual(r.status_code, 200)
        sharing = json.loads(r.data)
        self.assertEqual(len(sharing), 2)
        self.assertEqual(sharing[1]['quicktext_id'], qt.id)
        self.assertEqual(sharing[1]['user_id'], user_id)
        self.assertEqual(sharing[1]['target_user_id'], target_user_id)

    def test_delete_share(self):
        user_id = 1
        with app.app_context():
            target_user = umodels.User.create(app, 'test2@quicktext.io', '1111')
            target_user_id = target_user.id

        self.login("test@quicktext.io", "test")

        r = self.delete('share/XXX')
        self.assertEqual(r.status_code, 404)

        quicktext = {
            "user": umodels.User.query.get(user_id),
            "data": {
                "title": 'test',
                "subject": 'subject_test',
                "tags": 'tag1, tag2',
                "shortcut": 'cut',
                "body": "Hello World"
            }
        }

        qt = qmodels.Quicktext.create(**quicktext)

        qt_sharing = qmodels.QuicktextSharing()
        qt_sharing.quicktext_id = qt.id
        qt_sharing.user_id = user_id
        qt_sharing.target_user_id = target_user_id

        db.session.add(qt_sharing)
        db.session.commit()

        r = self.delete('share/%s?quicktext_sharing_id=%d' % (
            qt.id, qt_sharing.id))
        self.assertEqual(r.status_code, 200)
        sharings = qmodels.QuicktextSharing.query.all()
        self.assertEqual(len(sharings), 1)
        self.assertEqual(sharings[0].deleted, 1)

    def test_get_multiple(self):
        """ Given a list of quicktext id's get it's sharing information """

        user_id = 1
        target_user_id = 2

        with app.app_context():
            umodels.User.create(app, "test2@test.com", "test")

        quicktext1 = {
            "user": umodels.User.query.get(user_id),
            "data": {
                "title": 'test',
                "subject": 'subject_test',
                "tags": 'tag1, tag2',
                "shortcut": 'cut',
                "body": "Hello World"
            }
        }
        quicktext2 = {
            "user": umodels.User.query.get(user_id),
            "data": {
                "title": 'test',
                "subject": 'subject_test',
                "tags": 'tag1, tag2',
                "shortcut": 'cut',
                "body": "Hello World"
            }
        }
        qt1 = qmodels.Quicktext.create(**quicktext1)
        qt2 = qmodels.Quicktext.create(**quicktext2)

        sharing1 = qmodels.QuicktextSharing()
        sharing1.quicktext_id = qt1.id
        sharing1.user_id = user_id
        sharing1.created_datetime = datetime.utcnow()
        sharing1.target_user_id = target_user_id

        sharing2 = qmodels.QuicktextSharing()
        sharing2.quicktext_id = qt2.id
        sharing2.user_id = user_id
        sharing2.created_datetime = datetime.utcnow()
        sharing2.target_user_id = target_user_id

        db.session.add(sharing1)
        db.session.add(sharing2)
        db.session.commit()

        self.login("test@quicktext.io", "test")

        r = self.post('share', data={'quicktext_ids': [
            qt1.id,
            qt2.id,
        ]})
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        # 2 owners and 2 shares with other users
        self.assertEqual(len(data['acl']), 4)

    def test_update_multiple(self):
        user_id = 1
        target_user_id = 2

        with app.app_context():
            umodels.User.create(app, "test2@test.com", "test")

        quicktext1 = {
            "user": umodels.User.query.get(user_id),
            "data": {
                "title": 'test',
                "subject": 'subject_test',
                "tags": 'tag1, tag2',
                "shortcut": 'cut',
                "body": "Hello World"
            }
        }
        quicktext2 = {
            "user": umodels.User.query.get(user_id),
            "data": {
                "title": 'test',
                "subject": 'subject_test',
                "tags": 'tag1, tag2',
                "shortcut": 'cut',
                "body": "Hello World"
            }
        }

        qt1 = qmodels.Quicktext.create(**quicktext1)
        qt2 = qmodels.Quicktext.create(**quicktext2)

        self.login("test@quicktext.io", "test")

        # give the target_user_id all permissions
        update_data = {
            "acl": {
                qt1.id: {
                    target_user_id: "all",
                },
                qt2.id: {
                    target_user_id: "all"
                }
            }
        }
        self.put("share", data=json.dumps(update_data), content_type="application/json")
