import base64

from q.q_app import app
from q.testing import QTestCase


class ApiTestCase(QTestCase):
    """ Convenience methods for API test cases """

    def setUp(self):
        super(ApiTestCase, self).setUp()
        self.c = app.test_client()
        self.email = ""
        self.password = ""

        self.base_url = '/api/1/'

    def get(self, url, *args, **kw):
        return self._open("GET", url, *args, **kw)

    def post(self, url, *args, **kw):
        return self._open("POST", url, *args, **kw)

    def put(self, url, *args, **kw):
        return self._open("PUT", url, *args, **kw)

    def delete(self, url, *args, **kw):
        return self._open("DELETE", url, *args, **kw)

    def head(self, url, *args, **kw):
        return self._open("HEAD", url, *args, **kw)

    def _open(self, method, url, *args, **kw):
        headers = kw.pop('headers', {})
        if self.email and self.password:
            headers['Authorization'] = 'Basic ' + \
                                       base64.b64encode("%s:%s" % (self.email, self.password))
        return self.c.open(self.base_url + url, method=method,
                           headers=headers, *args, **kw)


    def login(self, email, password):
        self.email = email
        self.password = password