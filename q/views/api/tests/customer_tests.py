import json
from datetime import datetime

from q.q_app import app, db
from q.models import customer as cmodels
from q.models import user as umodels
from q.models import quicktext as qmodels
from .core import ApiTestCase


class SubscriptionResourceTestCase(ApiTestCase):
    def test_get(self):
        self.login("test@quicktext.io", "test")
        # by default a registered customer has an payed subscription
        r = self.get("subscriptions")

        self.assertEquals(r.status_code, 200)
        data = json.loads(r.data)[0]
        self.assertEquals(data["plan"]['sku'], "yearly-eur-1")
        self.assertEquals(data["active"], True)


    def test_create_subscription(self):
        """ Create subscriptions """

        self.login("test@quicktext.io", "test")

        # invalid JSON
        r = self.post("subscriptions", "XXXX")
        self.assertEquals(r.status_code, 400)
        self.assertEquals(json.loads(r.data), "Invalid Value: No JSON object could be decoded")

        r = self.post("subscriptions", data=json.dumps({
            "sku": "XXXX"
        }), content_type="application/json")
        self.assertEquals(r.status_code, 400)
        self.assertTrue("Missing" in r.data, r.data)

        r = self.post("subscriptions", data=json.dumps({
            "sku": "XXXX",
            "quantity": "21",
            "token": "YYYYY",
        }), content_type="application/json")
        self.assertTrue("Invalid sku" in r.data, r.data)

        # stripe id of the customer is missing, but the user should not know about this
        plan = app.config.get("ACTIVE_PLANS")[3]

        # add a fake stripe_id and get an error
        customer = cmodels.Customer.query.get(1)
        customer.stripe_id = "fake id"
        db.session.add(customer)
        db.session.commit()

        r = self.post("subscriptions", data=json.dumps({
            "sku": plan['sku'],
            "quantity": 1,
            "token": "YYYYY" + plan['sku'],
        }), content_type="application/json")
        self.assertEquals(r.status_code, 500)
        self.assertEquals(json.loads(r.data), "Failed to validate payment. Please try again later.")


        # finally add a subscription manually and try again
        sub = cmodels.Subscription()
        sub.plan = 'trial'
        sub.active = True
        sub.customer_id = 1
        sub.end_datetime = datetime.utcnow()
        db.session.add(sub)
        db.session.commit()

        r = self.post("subscriptions", data=json.dumps({
            "sku": plan['sku'],
            "quantity": 1,
            "token": "YYYYY" + plan['sku'],
        }), content_type="application/json")
        self.assertEquals(r.status_code, 500)
        self.assertEquals(json.loads(r.data), "Failed to validate payment. Please try again later.")


class PlanResourceTestCase(ApiTestCase):
    def test_get(self):
        r = self.get("plans")
        self.assertEquals(r.status_code, 401)

        self.login("test@quicktext.io", "test")
        r = self.get("plans")
        self.assertEquals(r.status_code, 200)
        data = json.loads(r.data)
        self.assertTrue('email' in data)
        self.assertTrue('plans' in data)
        self.assertTrue('stripe_key' in data)
        self.assertTrue('preferred_currency' in data)


class MemberListResourceTestCase(ApiTestCase):
    def test_get(self):
        """ Simple test. Try to get members.
        Because there are no members, should return empty list."""
        r = self.get("members")
        self.assertEquals(r.status_code, 401)

        self.login("test@quicktext.io", "test")
        r = self.get("members")
        self.assertEquals(r.status_code, 200)
        # is empty because there are no members
        self.assertEquals(json.loads(r.data), [])

    def test_post(self):
        """ Create a new member and the get the list of members"""

        # forbidden
        r = self.post("members")
        self.assertEquals(r.status_code, 401)

        # empty form -> validation failure
        self.login("test@quicktext.io", "test")
        r = self.post("members")
        self.assertEquals(r.status_code, 400)
        rdata = json.loads(r.data)
        self.assertEquals(rdata['name'], ['This field is required.'])
        self.assertEquals(rdata['email'], ['This field is required.'])

        # missing fields -> validation failure
        r = self.post("members", data={'name': 'Hello'})
        self.assertEquals(r.status_code, 400)
        rdata = json.loads(r.data)
        self.assertTrue('name' not in rdata)
        self.assertEquals(rdata['email'], ['This field is required.'])

        # Used the same e-mail as the logged in user
        r = self.post("members", data={
            'name': 'Bobby Tables',
            'email': 'test@quicktext.io'
        })
        self.assertEquals(r.status_code, 400)
        rdata = json.loads(r.data)
        self.assertEquals(rdata, {
            'email': ["You can't add yourself as a member. Use one of your team member's e-mail."]
        })

        # finally a successful creation of a member
        r = self.post("members", data={
            'name': 'Bobby Tables',
            'email': 'some-email@quicktext.io'
        })
        self.assertEquals(r.status_code, 201)
        rdata = json.loads(r.data)
        self.assertEquals(rdata, "")

        # Check if the data is actually in the db
        members = cmodels.Member.query.all()
        self.assertEquals(len(members), 1)
        self.assertEquals(members[0].user_id, 2)  # the new user
        self.assertEquals(members[0].customer_id, 1)  # the user that created this user
        self.assertTrue(isinstance(members[0].created_datetime, datetime))

        # also check if a new user was created
        users = umodels.User.query.all()
        self.assertEquals(len(users), 2)
        self.assertEquals(users[1].email, "some-email@quicktext.io")

        # TODO: Check if e-mail was sent. For some reason catching e-mails doesn't work at the moment.

        # and finally check the current subscription was changed as well
        subscriptions = cmodels.Subscription.query.all()
        self.assertEquals(len(subscriptions), 1)
        self.assertEquals(subscriptions[0].customer_id, 1)
        self.assertEquals(subscriptions[0].quantity, 2)

    def test_share_all(self):
        """make sure that if that user checked 'share_all' checkbox in his user settings,
        all existing quicktexts have been shared
        """
        self.login("test@quicktext.io", "test")
        user = umodels.User.query.get(1)
        qt = qmodels.Quicktext.create(user, {
            'title': "qt title",
            'body': "qt body",
            'tags': '',
            'shortcut': '',
            'subject': ''
        })

        r = self.post("members", data={
            'name': 'Bobby Tables',
            'email': 'some-email@quicktext.io'
        })
        self.assertEquals(r.status_code, 201)
        rdata = json.loads(r.data)
        self.assertEquals(rdata, "")

        sharing = qmodels.QuicktextSharing.query.all()
        self.assertEquals(len(sharing), 0)

        user_info = umodels.UserInfo()
        user_info.user_id = user.id
        user_info.key = 'share_all'
        user_info.value = 'true'
        db.session.add(user_info)
        db.session.commit()

        member = cmodels.Member.query.first()
        db.session.delete(member)
        db.session.commit()

        r = self.post("members", data={
            'name': 'Bobby Tables',
            'email': 'some-email@quicktext.io'
        })
        self.assertEquals(r.status_code, 201)
        rdata = json.loads(r.data)
        self.assertEquals(rdata, "")

        sharing = qmodels.QuicktextSharing.query.all()
        self.assertEquals(len(sharing), 1)

class MemberItemResourceTestCase(ApiTestCase):
    def test_get(self):
        """ Get a single member information """

    def test_delete(self):
        """ Delete member """

        # forbidden
        r = self.delete('members/1')
        self.assertEquals(r.status_code, 401)

        # not found
        self.login("test@quicktext.io", "test")
        r = self.delete('members/1')
        self.assertEquals(r.status_code, 404)

        # add a member than delete the guy
        r = self.post("members", data={
            'name': 'Bobby Tables',
            'email': 'some-email@quicktext.io'
        })
        self.assertEquals(r.status_code, 201)
        rdata = json.loads(r.data)
        self.assertEquals(rdata, "")

        members = cmodels.Member.query.all()
        self.assertEquals(len(members), 1)
        member = members[0]
        user_info = member.user.info()
        self.assertEquals(user_info['name'], 'Bobby Tables')

        r = self.delete('members/%d' % member.id)
        self.assertEqual(r.status_code, 200)
        self.assertEquals(cmodels.Member.query.count(), 0)
