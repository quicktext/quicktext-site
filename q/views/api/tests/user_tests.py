import json
from .core import ApiTestCase

class AccountTestCase(ApiTestCase):
    def test_get(self):
        r = self.get("account")
        self.assertEquals(r.status_code, 401) # unauthorized

        self.login("test@quicktext.io", "test")
        r = self.get("account")
        self.assertEquals(r.status_code, 200) # unauthorized
        data = json.loads(r.data)
        self.assertEquals(data['id'], 1)
        self.assertEquals(data['email'], "test@quicktext.io")
        self.assertTrue(isinstance(data['info'], dict))

    def test_login_info(self):
        r = self.get("login-info")
        self.assertEquals(r.status_code, 200)
        self.assertEquals(json.loads(r.data)["is_loggedin"], False)

        self.login("test@quicktext.io", "test")
        r = self.get("login-info")
        self.assertEquals(r.status_code, 200)
        self.assertEquals(json.loads(r.data)["is_loggedin"], True)
