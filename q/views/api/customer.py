import json
import decimal
import calendar
from datetime import datetime

from flask import request, current_app
from flask import url_for
from flask.ext.security import current_user
from flask.ext.restful import reqparse, Resource
import stripe

from q.q_app import app, api, db, sentry
from q.models import customer as cmodels
from q.models import user as umodels
from q.models import quicktext as qmodels
from .utils import authenticate
from q.forms import MemberForm
from q.utils import generate_password
from q import tasks


parser = reqparse.RequestParser()


class CResource(Resource):
    """ Generic sutomer resource """
    decorators = [authenticate()]


class SubscriptionResource(CResource):
    def get(self):
        """ Get all subscriptions """

        # If not a customer then don't return any subscriptions
        if not current_user.customer:
            return []

        subscriptions = cmodels.Subscription.query. \
            filter_by(customer_id=current_user.customer.id). \
            order_by(cmodels.Subscription.created_datetime.desc()).all()

        ret = []
        for sub in subscriptions:
            ret.append({
                'id': sub.id,
                'plan': app.config["PLANS"][sub.plan],
                'quantity': sub.quantity,
                'active': sub.active,
                'created_datetime': sub.created_datetime,
                'start_datetime': sub.start_datetime,
                'end_datetime': sub.end_datetime,
                'canceled_datetime': sub.canceled_datetime
            })
        return ret

    def _can_upgrade(self, active_subscription, plan, quantity):
        """ See if customer can add more members """

        if not active_subscription:
            return False, 200

        # a bonus user can upgrade
        if active_subscription.plan in ('bonus', ):
            return True, 200

        # only if the quantity doesn't change
        if active_subscription.quantity == quantity:
            # can upgrade monthly -> yearly, but not the reverse.
            if active_subscription.plan != plan:
                active_plan = app.config['PLANS'][active_subscription.plan]
                new_plan = app.config['PLANS'][plan]
                if active_plan['interval'] == 'month' and new_plan['interval'] == 'year':
                    return True, 200
                else:
                    return "To switch to a monthly subscription send an e-mail to support@quicktext.io", 400

        # can reduce the quantity if the numbers of members allow it
        if active_subscription.quantity >= quantity:  # more members
            return True, 200
        elif active_subscription.quantity < quantity:  # see if customer can downgrade
            member_count = cmodels.Member.query.filter_by(customer_id=current_user.customer.id).count()
            if member_count >= quantity:
                return True, 200
            else:
                return "You must delete a member in order to downgrade", 400

        return "You can only upgrade if you change the number of team members.", 400

    def post(self):
        """ Add a new subscription.

        """

        try:
            data = json.loads(request.data)
            token = data['token']
            plan = data['sku']  # plan id
            quantity = int(data['quantity'])  # for how many users?

            assert plan in [p['sku'] for p in app.config.get("ACTIVE_PLANS")]
        except KeyError:
            return "Missing one of the fields: token, sku, quantity", 400
        except AssertionError:
            return "Invalid sku %s" % data["sku"], 400
        except ValueError as e:
            return "Invalid Value: %s" % str(e), 400

        active_subscription = cmodels.Subscription.query. \
            filter_by(customer_id=current_user.customer.id). \
            filter_by(active=True).first()

        update_subscription = False
        message_or_bool, response_code = self._can_upgrade(active_subscription, plan, quantity)
        if response_code != 200:
            return message_or_bool, response_code
        elif message_or_bool is True:
            update_subscription = True

        # get trial if any
        trial = cmodels.Subscription.query. \
            filter_by(customer_id=current_user.customer.id). \
            filter_by(active=True). \
            filter_by(plan='trial'). \
            order_by(cmodels.Subscription.end_datetime).first()

        # by default a subscription starts now
        trial_end = "now"
        if trial:
            # a UNIX timestamp - always UTC
            trial_end = str(calendar.timegm(trial.end_datetime.utctimetuple()))

        # should never happen, but none the less, verify
        stripe_customer = None
        stripe_id = current_user.customer.stripe_id
        if stripe_id is None:
            if not app.config.get('TESTING'):
                stripe_customer = stripe.Customer.create(
                    email=current_user.customer.user.email,
                    metadata={
                        'user_id': current_user.id,
                    },
                )
                stripe_customer = stripe_customer.id
                current_user.customer.stripe_id = stripe_customer.id
                db.session.add(current_user.customer)
                db.session.commit()

        try:
            if not stripe_customer:
                stripe_customer = stripe.Customer.retrieve(stripe_id)
            # Use Stripe's bindings...
            if update_subscription:
                # Update a subscription
                stripe_subscription = stripe_customer.subscriptions.retrieve(active_subscription.stripe_id)
                stripe_subscription.plan = plan
                stripe_subscription.quantity = quantity
                stripe_subscription.save()

                active_subscription.plan = plan
                active_subscription.quantity = quantity
                active_subscription.customer_id = current_user.customer.id
                active_subscription.stripe_id = stripe_subscription.id
                active_subscription.updated_datetime = datetime.utcnow()

                db.session.add(active_subscription)
                db.session.commit()
            else:
                # add a subscription
                stripe_subscription = stripe_customer.subscriptions.create(
                    plan=plan,
                    card=token['id'],
                    trial_end=trial_end,
                    quantity=quantity)

                subscription = cmodels.Subscription()
                subscription.plan = plan
                subscription.quantity = quantity
                subscription.customer_id = current_user.customer.id
                subscription.stripe_id = stripe_subscription.id
                subscription.created_datetime = datetime.utcnow()

                # make sure we start the subscription at the right time
                if trial:
                    subscription.start_datetime = trial.end_datetime
                else:
                    subscription.active = True  # make active only if trial ended
                    subscription.start_datetime = datetime.utcnow()

                db.session.add(subscription)
                db.session.commit()
        except stripe.error.CardError as e:
            sentry.captureException()
            return 'Your card was rejected. Please use another card or try again later', 500
        except stripe.error.InvalidRequestError as e:
            # Invalid parameters were supplied to Stripe's API
            sentry.captureException()
            return 'Failed to validate payment. Please try again later.', 500
        except stripe.error.AuthenticationError as e:
            # Authentication with Stripe's API failed
            # (maybe you changed API keys recently)
            sentry.captureException()
            return 'Failed to validate payment. Please try again later.', 500
        except stripe.error.APIConnectionError as e:
            # Network communication with Stripe failed
            sentry.captureException()
            return 'Failed to validate payment. Please try again later.', 500
        except stripe.error.StripeError as e:
            # Display a very generic error to the user, and maybe send
            # yourself an email
            sentry.captureException()
            return 'Failed to validate payment. Please try again later.', 500
        except Exception as e:
            sentry.captureException()
            return 'Failed to validate payment. Please try again later.', 500

        return '', 201

    def put(self):
        """ Update an existing subscription

        Update quantity or cancel subscription

        """


class PlanResource(CResource):
    """ Get plans information  """

    def get(self):
        """ Get available plans, stripe_key, preferred currency, etc.."""

        plans_by_currency = app.config['ACTIVE_PLANS_BY_CURRENCY']

        for currency, plans in plans_by_currency.items():
            for plan in plans:
                a = decimal.Decimal(plan['amount'])
                if plan['interval'] == 'year':
                    a = a / 12  # get the montly price
                plan['tens'] = int(a - a * 100 % 100 / 100)
                plan['cents'] = int((a - plan['tens']) * 100)

        return {
                   'email': current_user.email,
                   'stripe_key': app.config['STRIPE_PUBLIC'],
                   'preferred_currency': 'eur',  # TODO: use Geoip someday to detect currencies
                   'plans': plans_by_currency
               }, 200


def _serialize_member(user):
    user_info = user.info()
    member_id = user.member.id if user.member else ''
    return {
        "id": member_id,
        "user_id": user.id,
        "name": user_info.get('name', ''),
        "email": user.email,
    }


class MemberListResource(CResource):
    def get(self):
        """ Get all members for this account """
        users = current_user.member_users()
        return map(_serialize_member, users), 200

    def post(self):
        """ Add a new member

        Conditions:

        Can add a new member only if in trial or if has an active subscription with the the right number of members

        """

        if current_user.member:  # members cannot add other members
            return """Cannot add a member. Only account owners can add new members.
                      Please contact your supervisor or support@quicktext.io.""", 400

        form = MemberForm(csrf_enabled=False)
        if form.validate():
            if form.data['email'] == current_user.email:
                return {"email": ["You can't add yourself as a member. Use one of your team member's e-mail."]}, 400

            active_sub = current_user.customer.active_subscription()
            if not active_sub:  # if there is no active plan, no more members can be added
                return """Your subscription is no longer active. Please subscribe.""", 400

            existing_members = current_user.member_users()
            # count this user too
            if len(existing_members) + 1 > active_sub.quantity:
                return "Maximum number of members reached. Please upgrade your subscription.", 400

            # if member already exists then don't add it
            if form.data['email'] in [m.email for m in existing_members]:
                return "", 200

            # Token used by member to login for the very first time.
            member_token = generate_password()
            current_user_info = current_user.info()
            email_data = {
                "base_url": app.config.get("BASE_URL"),
                "customer_name": current_user_info.get('name', ''),
                "login_link": url_for("member_login", **{"token": member_token}),
                "video_link": "https://www.youtube.com/watch?v=G1WZDdT9eNU",
                "help_link": "http://help.quicktext.io/src/00-getting-started.html",

                "member_name": form.data['name'],
                "email": form.data['email'],
                "password": ''
            }

            # Handle user creation/fetching
            user = umodels.User.query.filter_by(email=form.data['email']).first()
            if not user:  # there is no such user: create user then
                password = generate_password()
                email_data['password'] = password

                user = umodels.User.create(current_app, form.data['email'], password, extra={
                    "name": form.data['name'],
                    "member_token": member_token,
                    "locale": "en",  # TODO(alex): Support other locales later
                    "share_all": "true",
                })
            else:  # User exists, add the token for an existing user
                info = umodels.UserInfo()
                info.user_id = user.id
                info.key = 'member_token'
                info.value = member_token
                db.session.add(info)



            # add the member
            member = cmodels.Member()
            member.user_id = user.id
            member.customer_id = current_user.customer.id
            member.created_datetime = datetime.utcnow()
            db.session.add(member)
            db.session.commit()

            # Handle quicktexts
            user_info = current_user.info()
            if user_info.get('share_all') == 'true':
                # share all user's quicktexts with the new member
                quicktexts = set(qmodels.Quicktext.query.filter(qmodels.Quicktext.user_id == current_user.id).all())
                # then get all shared quicktexts
                quicktexts |= set(qmodels.Quicktext.all_shared(current_user.id))

                for quicktext in quicktexts:
                    sharing = qmodels.QuicktextSharing()
                    sharing.quicktext_id = quicktext.id
                    sharing.user_id = current_user.id
                    sharing.target_user_id = member.user_id
                    sharing.created_datetime = datetime.utcnow()
                    sharing.permission = 'edit'
                    db.session.add(sharing)

                db.session.commit()

            if form.data['send_notification']:
                tasks.send_mail.delay(subject="Welcome to Quicktext",
                                      sender="Alex Plugaru <support@quicktext.io>",
                                      recipients=[form.data['email']],
                                      template='emails/new_member.txt',
                                      email_data=email_data)

            return "", 201

        return form.errors, 400


class MemberItemResource(CResource):
    def get(self, member_id):
        """ Get a member information """
        member = cmodels.Member.query.filter_by(customer_id=current_user.customer.id).filter_by(id=member_id).first()
        if not member:
            return "", 404

        return _serialize_member(member.user), 200

    def put(self, member_id):
        """Update member details. Doen't really make sense."""

        raise NotImplementedError

    def delete(self, member_id):
        """ Delete a member. The member will lose the status it previously had in this case """

        member = cmodels.Member.query.filter_by(customer_id=current_user.customer.id).filter_by(id=member_id).first()
        if not member:
            return "", 404
        db.session.delete(member)
        db.session.commit()
        return "", 200

# Routing
api.add_resource(SubscriptionResource, '/api/1/subscriptions')
api.add_resource(PlanResource, '/api/1/plans')
api.add_resource(MemberListResource, '/api/1/members')
api.add_resource(MemberItemResource, '/api/1/members/<int:member_id>')
