import json
import datetime
from flask import make_response
from flask.ext.restful import Api


class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)


def output_json(data, code, headers=None):
    """Makes a Flask response with a JSON encoded body"""
    resp = make_response(json.dumps(data, cls=JSONEncoder), code)
    resp.headers.extend(headers or {})
    return resp


class QApi(Api):
    """ Generic API """

    def __init__(self, *args, **kw):
        super(QApi, self).__init__(*args, **kw)
        self.representations = {
            'application/json': output_json
        }