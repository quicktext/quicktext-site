from functools import wraps

from flask import request
from flask.ext.restful import abort
from flask.ext.security import current_user, login_user
from flask.ext.security.forms import LoginForm
from werkzeug.datastructures import MultiDict


def authenticate(required=True):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if not current_user.is_anonymous():
                return func(*args, **kwargs)
            auth = request.authorization
            if auth and auth.username and auth.password:
                if try_login(auth.username, auth.password):
                    return func(*args, **kwargs)
                    # finally if authentication is not required
            if not required:
                return func(*args, **kwargs)
            abort(401)

        return wrapper

    return decorator

def is_staff():
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if not current_user.is_anonymous() and current_user.is_staff():
                return func(*args, **kwargs)
            abort(401)

        return wrapper

    return decorator


def try_login(email, password):
    """ Try to login user"""

    form = LoginForm(MultiDict({
        "email": email, "password": password}),
                     csrf_enabled=False)
    # try to login and remember
    return form.validate() and login_user(form.user, True)
