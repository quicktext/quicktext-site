""" Public views. No login required. """
from time import time

from flask import render_template, request, current_app, redirect, session, abort
from flask import url_for
from flask.ext.security import login_user, current_user
from flask.ext.security.forms import LoginForm, ForgotPasswordForm
from flask.ext.security.recoverable import send_reset_password_instructions
from flask.ext.social import login_failed as social_login_failed
from flask.ext.social.utils import get_provider_or_404
from flask.ext.social.views import connect_handler
from flask.ext.mail import Mail, Message
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import cast
from sqlalchemy.types import Integer

from q.q_app import app, db, customerio, sentry, celery
from q import forms, tasks
from q.models import user as umodels
from q.models import quicktext as qmodels
from q.utils import divide_time


@app.route('/')
def index():
    plans = app.config.get("ACTIVE_PLANS_BY_CURRENCY")["eur"]  # eur for now - add localization later
    for plan in plans:
        a = plan['amount']
        if plan['interval'] == 'year':
            a = a / 12  # get the montly price
        plan['tens'] = int(a - a * 100 % 100 / 100)
        plan['cents'] = int((a - plan['tens']) * 100)

    # give our odometer some values
    wpm = app.config.get("AVG_WPM")
    word_count = db.session.query(
        func.sum(cast(qmodels.QuicktextStats.value, Integer)).label('words')).filter(
        qmodels.QuicktextStats.key == 'words').first()
    word_count = word_count[0]
    if not word_count:
        saved_time = 0
    else:
        saved_time = round(word_count / wpm)
    times = divide_time(saved_time)

    registration_form = forms.RegisterForm()
    if 'failed_login_connection' in session:
        data = session['failed_login_connection']
        registration_form = forms.RegisterForm(
            given_name=data.get("given_name"),
            family_name=data.get("family_name"),
            email=data.get("email"),
        )

    return render_template('public/index.html',
                           plans=plans,
                           word_count=word_count,
                           saved_time=saved_time,
                           times=times,
                           login_user_form=LoginForm(),
                           registration_form=registration_form,
                           provider_id=session.get("provider_id", ""),
                           forgot_form=ForgotPasswordForm(), )


@app.route('/privacy')
def privacy():
    return render_template('public/privacy.html')


@app.route('/signin', methods=['GET', 'POST'])
def signin():
    if current_user.is_authenticated():
        return redirect(url_for('auth_index'))

    form = LoginForm()
    if form.validate_on_submit():
        if login_user(form.user, True):  # and remember them
            return redirect(url_for('auth_index'))
    return redirect(url_for("index") + "?message=Login failed, please try again!#login")


@app.route('/forgot', methods=['GET', 'POST'])
def forgot():
    if current_user.is_authenticated():
        return redirect(url_for('auth_index'))

    form = ForgotPasswordForm()
    if form.validate_on_submit():
        send_reset_password_instructions(form.user)
        return redirect(url_for("index") + "?success=Check your e-mail! We've sent the recovery instructions.#forgot")
    return redirect(url_for("index") + "?message=Your e-mail was not found! Please try again.#forgot")


@app.route('/signup/<plan>', methods=['GET', 'POST'])
def signup(plan):
    """ Show a registration form with a plan """

    if current_user.is_authenticated():
        return redirect(url_for('auth_index'))

    plans = app.config.get("PLANS")
    stripe_key = app.config.get("STRIPE_PUBLIC")

    if plan not in plans:
        abort(404)

    form = forms.RegisterForm()

    if request.method == 'GET':
        coupon = request.args.get('coupon')
        if coupon:
            form = forms.RegisterForm(coupon=coupon)

    if request.method == 'POST':
        if form.validate_on_submit():
            extra = {
                'name': form.name.data,
                'share_all': "true"
            }

            customer_data = {
                "stripe_token": form.stripe_token.data,
                "quantity": form.quantity.data,
                "plan": plan
            }
            user = umodels.User.create(current_app, form.email.data, form.password.data,
                                       extra=extra, customer_data=customer_data)

            user_info = user.info()

            tasks.customerio_register.delay({
                'user_id': user.id,
                'email': form.email.data,
                'name': user_info['name'],
                'given_name': user_info['given_name'],
                'family_name': user_info['family_name'],
                'quantity': form.quantity.data,
                'plan': plan,
            })

            if login_user(user):
                return redirect(url_for('auth_index'))

    return render_template("public/signup.html",
                           plan=plans[plan],
                           stripe_key=stripe_key,
                           form=form)


# XXX(alexp): Don't use this anymore! Use signin instead. Left this for socialauth stuff.
@app.route('/registration', methods=['GET', 'POST'])
@app.route('/registration/<provider_id>', methods=['GET', 'POST'])
def registration(provider_id=None):
    # XXX(alexp): Don't use this anymore! Use signup instead. Left this for socialauth stuff.
    if current_user.is_authenticated():
        return redirect(url_for('auth_index'))

    return redirect(url_for('signup'))

    #if provider_id:
    #    provider = get_provider_or_404(provider_id)
    #    connection_values = session.get('failed_login_connection', None)
    #else:
    #    provider = None
    #    connection_values = None

    #form = forms.RegisterForm()

    #if form.validate_on_submit():
    #    extra = {
    #        'given_name': form.given_name.data,
    #        'family_name': form.family_name.data,
    #        'share_all': "true"
    #    }

    #    user = umodels.User.create(current_app, form.email.data,
    #                               form.password.data, extra=extra)

    #    mailer = Mail(current_app)
    #    # Send a welcome message
    #    msg = Message("Welcome to Quicktext!",
    #                  sender="Alex Plugaru <support@quicktext.io>",
    #                  recipients=[form.data['email']])
    #    # TODO: HTML e-mail?
    #    msg.body = render_template("emails/registration.txt", **extra)
    #    mailer.send(msg)

    #    # Track an action in the customerio
    #    # TODO: do this in a thread instead at some point - it's a blocking operation call to a remote API

    #    try:
    #        customerio.identify(
    #            id=user.id,
    #            created_at=int(time()),
    #            email=form.email.data,
    #            name="%s %s" % (form.given_name.data, form.family_name.data),
    #            given_name=form.given_name.data,
    #            family_name=form.family_name.data,
    #            plan="trial")
    #        customerio.track(customer_id=user.id, name="registered")
    #    except:
    #        sentry.captureException("Failed to register user with customerio")

    #    # See if there was an attempted social login prior to registering
    #    # and if so use the provider connect_handler to save a connection
    #    connection_values = session.pop('failed_login_connection', None)

    #    if connection_values:
    #        connection_values['user_id'] = user.id
    #        connect_handler({
    #                            'provider_id': 'google',
    #                            'provider_user_id': connection_values['id'],
    #                            'user_id': user.id,
    #                            'access_token': connection_values['access_token']
    #                        }, provider)
    #    if login_user(user):
    #        return redirect(url_for('auth_index'))

    #    return render_template('public/thanks.html', user=user)

    #login_failed = int(request.args.get('login_failed', 0))
    #if form.errors:
    #    message = ""
    #    for field, errors in form.errors.items():
    #        label = getattr(form, field).label.text
    #        message += "%s: %s<br />" % (label, ", ".join(errors))
    #    return redirect(url_for('index') + "?message=%s#registration" % message)
    #return redirect(url_for('index'))


@app.route("/member-login/<token>", methods=["GET"])
def member_login(token):
    """ First login entry point for a member.

    A member was invited to use quicktext and he should be able to login to his account
    by using a simple link.

    """

    # use UserInfo to store the tokens
    info = umodels.UserInfo.query.filter_by(key="member_token").filter_by(value=token).first()
    if info:
        db.session.delete(info)
        db.session.commit()
        user = umodels.User.query.get(info.user_id)
        if login_user(user):
            return redirect(url_for("auth_index"))
    return redirect(url_for("index") + "?message=Login failed, please try again!#login")


@app.errorhandler(404)
def page_not_found(e):
    return render_template('public/404.html'), 404


class SocialLoginError(Exception):
    def __init__(self, provider):
        self.provider = provider
        session["provider_id"] = provider.id


@social_login_failed.connect_via(app)
def on_login_failed(sender, provider, oauth_response):
    app.logger.debug('Social Login Failed via %s; '
                     '&oauth_response=%s' % (provider.name, oauth_response))

    # Save the oauth response in the session so we can make the connection
    # later after the user possibly registers
    import httplib2
    import oauth2client.client as googleOauth
    import apiclient.discovery as googleApi

    def get_api():
        http = httplib2.Http()
        credentials = googleOauth.AccessTokenCredentials(
            access_token=oauth_response['access_token'],
            user_agent='')
        http = credentials.authorize(http)
        return googleApi.build('oauth2', 'v2', http=http)

    data = get_api().userinfo().get().execute()
    data['access_token'] = oauth_response['access_token']
    # this get's all the user information we need. Fullname, email, etc..
    session['failed_login_connection'] = data

    raise SocialLoginError(provider)


@app.errorhandler(SocialLoginError)
def social_login_error(error):
    return redirect(
        url_for('index') + '?success=Great! Now complete this form and you\'re done!#registration')
