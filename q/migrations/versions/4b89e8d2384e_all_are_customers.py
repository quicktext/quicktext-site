"""All are customers

Meaning that in prod all are customers for now. This is needed to so that we

Revision ID: 4b89e8d2384e
Revises: 11c761d8a59c
Create Date: 2014-07-01 21:40:41.795590

"""

# revision identifiers, used by Alembic.
revision = '4b89e8d2384e'
down_revision = '11c761d8a59c'

from datetime import datetime
from q.models import customer as cmodels
from q.models import user as umodels
from q.q_app import db


def upgrade():
    """ All users in prod will become customers  """
    users = umodels.User.query.all()
    customers = cmodels.Customer.query.all()
    existing_user_ids = [c.user_id for c in customers]

    for user in users:
        if user.id not in existing_user_ids:
            customer = cmodels.Customer()
            customer.user_id = user.id
            customer.active = True
            customer.created_datetime = datetime.utcnow()
            db.session.add(customer)
    db.session.commit()

def downgrade():
    """ No downgrade for this step is possible"""
