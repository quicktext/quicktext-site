"""Added create_datetime to user

Revision ID: 4e9fdae7a3a1
Revises: 17cf01f4e97b
Create Date: 2014-01-27 22:03:43.926156

"""

# revision identifiers, used by Alembic.
revision = '4e9fdae7a3a1'
down_revision = '17cf01f4e97b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user', sa.Column('created_datetime', sa.DateTime(), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('user', 'created_datetime')
    ### end Alembic commands ###
