"""quicktext fields

Revision ID: 55740eefd69f
Revises: 4f46436008d0
Create Date: 2014-08-20 12:47:58.179865

"""

revision = '55740eefd69f'
down_revision = '4f46436008d0'

from collections import defaultdict
from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('quicktext',
        sa.Column('modified_datetime', sa.DateTime(), nullable=True))

    for col in ['body', 'shortcut', 'subject', 'tags', 'title']:
        op.add_column('quicktext', sa.Column(col, sa.String(), nullable=True))
        op.execute("UPDATE quicktext SET %s = ''" % col)
        op.alter_column('quicktext', col, nullable=False)

    conn = op.get_bind()

    query = (
        "SELECT quicktext_id, id FROM quicktext_version "
        "ORDER BY created_datetime DESC"
    )
    versions = defaultdict(list)
    for row in conn.execute(query):
        (q_id, version_id) = row
        versions[q_id].append(version_id)

    for q_id in versions:
        latest = versions[q_id][0]
        conn.execute(
            "UPDATE quicktext SET "
            "title = quicktext_version.title, "
            "tags = quicktext_version.tags, "
            "subject = quicktext_version.subject, "
            "shortcut = quicktext_version.shortcut, "
            "body = quicktext_version.body, "
            "modified_datetime = quicktext_version.created_datetime "
            "FROM quicktext_version "
            "WHERE quicktext.id = '%s' "
            "AND quicktext_version.id = %d"
            % (q_id, latest)
        )
        conn.execute("DELETE FROM quicktext_version WHERE id = %d" % latest)


def downgrade():
    conn = op.get_bind()
    for [q_id] in conn.execute("SELECT id FROM quicktext"):
        conn.execute(
            "INSERT INTO quicktext_version "
            "  (quicktext_id, created_datetime, user_id, "
            "   title, tags, subject, shortcut, body) "
            "SELECT "
            "   '{q_id}', modified_datetime, user_id, "
            "   title, tags, subject, shortcut, body "
            "FROM quicktext "
            "WHERE quicktext.id = '{q_id}'"
            .format(q_id=q_id)
        )

    for col in ['body', 'shortcut', 'subject', 'tags', 'title']:
        op.drop_column('quicktext', col)

    op.drop_column('quicktext', 'modified_datetime')
