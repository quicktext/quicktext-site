"""data as columns

Revision ID: 4f46436008d0
Revises: 4b89e8d2384e
Create Date: 2014-08-13 13:33:27.195010

"""

# revision identifiers, used by Alembic.
revision = '4f46436008d0'
down_revision = '3e7f9bb2618'

from alembic import op
import sqlalchemy as sa


def upgrade():
    for column in ['body', 'shortcut', 'subject', 'tags', 'title']:
        op.execute("UPDATE quicktext_data SET value = '' WHERE value is NULL")
        op.add_column(
            'quicktext_version',
            sa.Column(column, sa.String(), nullable=True),
        )

        query = (
            "WITH data AS ( "
            "  SELECT quicktext_version_id, value "
            "  FROM quicktext_data "
            "  WHERE key = '{column}' "
            ") "
            "UPDATE quicktext_version SET {column} = data.value FROM data "
            "WHERE data.quicktext_version_id = quicktext_version.id; "
        ).format(column=column)
        op.execute(query)

        op.alter_column('quicktext_version', column, nullable=False)

    op.drop_table('quicktext_data')


def downgrade():
    op.create_table('quicktext_data',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('quicktext_id', sa.String(length=40), nullable=True),
    sa.Column('quicktext_version_id', sa.Integer(), nullable=True),
    sa.Column('key', sa.String(length=255), nullable=True),
    sa.Column('value', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['quicktext_id'], ['quicktext.id'], ),
    sa.ForeignKeyConstraint(['quicktext_version_id'], ['quicktext_version.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index('ix_quicktext_data_key', 'quicktext_data', ['key'], unique=False)

    for column in ['body', 'shortcut', 'subject', 'tags', 'title']:
        query = (
            "INSERT INTO quicktext_data "
            "  (quicktext_id, quicktext_version_id, key, value) "
            "SELECT quicktext_id, id, '{column}', {column} "
            "FROM quicktext_version"
        ).format(column=column)
        op.execute(query)

        op.drop_column('quicktext_version', column)
