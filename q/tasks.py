import time

from flask import render_template
from flask.ext.mail import Message

from q_app import celery, customerio, sentry, mailer


@celery.task
def customerio_track(customer_id, name, **kw):
    """ Track some event for some customer with customer.io """
    customerio.track(customer_id=customer_id, name=name, **kw)


@celery.task
def customerio_register(data):
    customerio.identify(
        id=data['user_id'],
        created_at=int(time()),
        **data)
    customerio.track(customer_id=data['user_id'], name="registered")


@celery.task
def send_mail(subject, sender, recipients, template, email_data):
    msg = Message(subject,
                  sender=sender,
                  recipients=recipients)
    msg.body = render_template(template, **email_data)
    mailer.send(msg)

