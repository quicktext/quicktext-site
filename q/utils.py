import uuid
from datetime import datetime, timedelta
from decimal import Decimal
import re

from lxml import etree


def generate_password():
    """ Generate a new password for the user """
    return str(uuid.uuid4()).split('-')[0]


def parse_body(text):
    text = re.sub(r'\[\[TO=(\w+)]]', r'{{to.0.\1}}', text)
    return text


def parse_tags(menu_title):
    tag_list = [
        t for t in menu_title.split()
        if re.search(r'[\d\w]', t)  # only keep tags that contain alphanumerics
    ]
    return ', '.join(sorted(tag_list))


def parse_thunderbird_quicktext(stream):
    quicktexts = []

    try:
        tree = etree.parse(stream)
    except etree.XMLSyntaxError as e:
        raise ValueError(e)
    menus = tree.findall('menu')
    for menu in menus:
        menu_title = menu.find('title').text
        texts = menu.findall('.//text')
        for text in texts:
            qt = {
                'shortcut': getattr(text.find('keyword'), 'text', ''),
                'subject': getattr(text.find('subject'), 'text', ''),
                'title': getattr(text.find('name'), 'text', ''),
                'body': parse_body(getattr(text.find('body'), 'text', '')),
                'tags': parse_tags(menu_title)
            }

            quicktexts.append(qt)
    return quicktexts


def divide_time(seconds):
    """Take some seconds and divide then into minutes, hours.. years"""

    data = {
        'seconds': 0,
        'minutes': 0,
        'hours': 0,
        'days': 0,
        'years': 0
    }
    if seconds > 0:
        delta = timedelta(seconds=seconds)
        d = datetime(1, 1, 1) + delta
        data['seconds'] = d.second
        data['minutes'] = d.minute
        data['hours'] = d.hour
        data['days'] = d.day - 1
        data['years'] = d.year - 1
    return data


def reduce_numbers(n):
    """ Write nice numbers. Ex: 1000 -> 1k """

    if not n:
        return "0"
    if n < 1000:
        return n

    mag = ""
    p = 0
    if n < 10 ** 6:
        mag = "thousand"
        p = 10 ** 3
    elif n < 10 ** 8:
        p = 10 ** 6
        mag = "million"
    elif n < 10 ** 11:
        p = 10 ** 8
        mag = "billion"
    elif n < 10 ** 14:
        p = 10 ** 11
        mag = "trillion"

    n = Decimal(n)
    return "%.2f %s" % (Decimal((n / p) * p / p), mag)
