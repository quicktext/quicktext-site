{{customer_name}} shared {{quicktexts|length}} quicktext{% if quicktexts|length > 1 %}s{% endif %} with you:

{% for quicktext in quicktexts %}
{{quicktext.title}}: {{quicktext.body|truncate(200, True)}}
{% endfor %}

{% if message %}{{customer_name}} added a note:
{{message}}
{%endif%}

For any questions please reply to this e-mail or contact: support@quicktext.io
