Hello,

We received a request for resetting your password on our website.
If it wasn't you who requested this please ignore this e-mail.

Click the link below to reset your password:
{{ reset_link }}

Note: If you're having trouble resetting your password please contact us at: support@quicktext.io

Kind regards,
Alex.